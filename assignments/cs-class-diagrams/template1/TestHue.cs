﻿using _05_ClassDiagrams.Solution;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace _05_ClassDiagrams.Template;

[TestFixture]
public class TestHue
{
    [Test]
    public void End_To_End_Test()
    {
        //First test, Add a room
        //Arrange
        Hub hub = new Hub();
        Room kitchen = new Room("kitchen");

        //Act
        hub.AddRoom(kitchen);

        //Assert
        Assert.Multiple(() =>
        {
            Assert.That(kitchen.Name, Is.EqualTo("kitchen"));
            Assert.That(kitchen.ToString, Is.EqualTo("kitchen {\n}"));
            Assert.That(hub.GetRoom("kitchen"), Is.EqualTo(kitchen));
            Assert.That(hub.GetRoom("no such room"), Is.Null);
        });

        //Second test, Add a light
        //Arrange
        Light kitchenCeiling = new Light("ceiling");

        //Act
        kitchen.AddLight(kitchenCeiling);

        //Assert
        Assert.Multiple(() =>
        {
            Assert.That(kitchenCeiling.Name, Is.EqualTo("ceiling"));
            Assert.That(kitchenCeiling.ToString, Is.EqualTo("ceiling → l0"));
            Assert.That(kitchen.GetLight("ceiling"), Is.EqualTo(kitchenCeiling));
            Assert.That(kitchen.GetLight("No Such Light"), Is.Null);
            Assert.That(kitchen.ToString(), Is.EqualTo("kitchen {\n    ceiling → l0\n}"));
        });

        //Third test Brightness
        //Act
        kitchenCeiling.SetState(new LightState(42));

        //Assert
        Assert.That(kitchen.ToString(), Is.EqualTo("kitchen {\n    ceiling → l42\n}"));

        //Fourth test ColorLights
        //Arrange
        hub.AddRoom(new Room("bedroom"));
        Room bedroom = hub.GetRoom("bedroom")!;

        //Act
        bedroom.AddLight(new Light("bedside left"));
        bedroom.AddLight(new Light("bedside right"));
        bedroom.AddLight(new ColorLight("closet"));

        //Assert
        Assert.That(bedroom.ToString(), Is.EqualTo("bedroom {\n    bedside left → l0\n    bedside right → l0\n    closet → l0 h0 s0\n}"));

        //Fifth test Set Atmosphere
        //Arrange
        LightState deepRed = new ColorLightState(30, 324, 100);
        bedroom.SetState(deepRed);

        //Assert
        Assert.That(bedroom.ToString(), Is.EqualTo("bedroom {\n    bedside left → l30\n    bedside right → l30\n    closet → l30 h324 s100\n}"));

        //Act
        bedroom.GetLight("bedside right")!.SetState(new LightState(50));

        //Assert
        Assert.That(bedroom.ToString(), Is.EqualTo("bedroom {\n    bedside left → l30\n    bedside right → l50\n    closet → l30 h324 s100\n}"));

        //Sixth test Scenes
        bedroom.StoreScene("romantic reading");
        bedroom.SetState(new LightState(100));

        Assert.That(bedroom.ToString(), Is.EqualTo("bedroom {\n    bedside left → l100\n    bedside right → l100\n    closet → l100\n}"));
        bedroom.StoreScene("bright");

        bedroom.ActivateScene("romantic reading");
        Assert.That(bedroom.ToString(), Is.EqualTo("bedroom {\n    bedside left → l30\n    bedside right → l50\n    closet → l30 h324 s100\n}"));

        bedroom.ActivateScene("bright");
        Assert.That(bedroom.ToString(), Is.EqualTo("bedroom {\n    bedside left → l100\n    bedside right → l100\n    closet → l100\n}"));

        //Seventh test complete hub
        Assert.That(hub.ToString(), Is.EqualTo("Hub>>>\nkitchen {\n    ceiling → l42\n}bedroom {\n    bedside left → l100\n    bedside right → l100\n    closet → l100\n}<<<Hub"));
    }
}

# [(noun, strength, reliability, weight,)...]
var nouns = new[] {
    new { Name = "sword", Strength = 4, Reliability = 8, Weight = 5 },
    new { Name = "shield", Strength = 1, Reliability = 8, Weight = 8 },
    new { Name = "bow", Strength = 8, Reliability = 2, Weight = 3 },
    new { Name = "canon", Strength = 9, Reliability = 2, Weight = 15 },
    new { Name = "force field", Strength = 2, Reliability = 7, Weight = 1 },
    new { Name = "drone", Strength = 9, Reliability = 2, Weight = 20 },
    new { Name = "tank", Strength = 9, Reliability = 7, Weight = 45 }
};

// [(adjective, multiplier,)...]
var adjectives = new[] {
    new { Adjective = "wooden", Multiplier = 1 },
    new { Adjective = "bronze", Multiplier = 2 },
    new { Adjective = "steel", Multiplier = 3 },
    new { Adjective = "diamond", Multiplier = 4 },
    new { Adjective = "nanofiber", Multiplier = 5 },
    new { Adjective = "unobtainium", Multiplier = 6 },
    new { Adjective = "epic", Multiplier = 7 },
    new { Adjective = "legendary", Multiplier = 8 },
    new { Adjective = "eternal", Multiplier = 9 },
};

Random random = new();
var noun = nouns[random.Next(0, nouns.Length)];
var adjective = adjectives[random.Next(0,  adjectives.Length)];

Strength = noun.Strength * adjective.Multiplier / 5; // Apply the multiplier to strength
Name = $"{adjective.Adjective} {noun.Name}";
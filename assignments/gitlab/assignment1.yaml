- Introduction: |
    <wired-card>
      <img src="git-remote.png" class="no-border">
    </wired-card>
    
    In the previous assignment you saw how you can create a Git repository locally (on your own laptop or computer) and commit and revert changes. You have also seen how you can create branches locally and merge or delete them.

    A local Git repository is useful for keeping track of changes you make to your code but no so much if you want to collaborate with others. You can give others - your teammates for example - access to the source code via a so-called "remote repository". Remote repositories are located on the internet and depending on the rights, others can view and / or modify the code. The picture below shows the relationship between a local repository (on your laptop) and a remote repository.
    
    By now you should be familiar with terms like "Working copy", "Staging area" and your "Local Repository". Using 'git add' you move your changes from the working directory to the staging area. You can save these changes to your local repository with 'git commit'. With a remote repository, you have to go the extra mile to sync your local repository with the remote repository (for example, a repository on GitLab). You do this with the help of 'git push'. To update your local repository with the changes from the remote repository, use 'git pull' or 'git fetch'.

- Tasks:
  - Create a GitLab account: |
      For this assignment you need to have access to a remote repositories on GitLab. Go to [gitlab.com](http://gitlab.com) and sign up for a free account (use your Saxion email address). With this account you can create, clone, push and pull repositories. Be sure to select the role 'Software Developer' in order to get the proper configuration for your account. 
      
      You can skip the parts for creating a new project or organization.

  - Create a SSH key for your GitLab account: |
      There are two ways to access a remote repository, using HTTPS and SSH. The latter is recommended so generate a new SSH key pair and add it your GitLab account. 

      You can generate a key as follows (skip the passphrase):
      ```sh
      ssh-keygen -t ed25519 -C "Captain Awesome's MacBook key"
      ```
      Also see the documentation [here](https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair) on how to generate SSH keys.

      Instruction on how to add your (public) key to GitLab can be found [here](https://docs.gitlab.com/ee/ssh/index.html#add-an-ssh-key-to-your-gitlab-account)


- Assignment:
  - Create a git repository:
    -
      text: |
        Your first task is to create a blank repository in GitLab. The easiest way to achieve this is by going to [gitlab.com](http://gitlab.com) using your browser and create a new project. Select "Create blank project" from the options. Be sure that your repository is private and that you initialize it with a README.

        Once you have successfully created your repository then clone it so you can access the code locally on your laptop. You should use the "Clone with SSH" option so that you can push and pull your changes without using a password.

        Note that when cloning the repository you have the option to open it in you IDE "Visual Studio Code (SSH)". For now you should manually clone the repository so you know how this works.
      ^merge: feature
      weight: 0.5

  - Push it real good:
    -
      link: https://www.atlassian.com/git/tutorials/syncing/git-push
      title: git push
      info: The git push command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo.
    - 
      text: |
        Your repository should have a README.md file. If this file does not exist create it locally on your laptop. Add the following text to the README.md:
        ```
        # Simple web scraper

        This repository contains a Python implementation for scraping a website. 

        Please note that scraping is not appreciated by many websites. Read the terms and conditions before you start scraping.
        ```
        
        Commit the change with commit message "1. Edit README.md". Push your changes to the remote repository and check whether they are visible on www.gitlab.com.

      ^merge: feature
      weight: 0.5

  - Tug of war:
    -
      link: https://www.atlassian.com/git/tutorials/syncing/git-pull
      title: git pull
      info: The git pull command is used to fetch and download content from a remote repository and immediately update the local repository to match that content.
    - 
      text: |
        We are going to do a simple simulation of what happens when something changes on remote and conflicts with your current changes. Reproduce the following steps:
        1. Go to www.gitlab.com and select your repository.
        2. Select the README.md file and click on edit.
        3. Insert the following line to the README.md file "This repository contains code for scraping articles on nu.nl"
        4. Commit your changes with the following commit message "2.a Update README.md".
        5. On your laptop you should edit the README.md file. Don't pull the latest change just yet.
        6. Insert the following line to the README.md file "This repository contains code for scraping articles on tweakers.net"
        7. Commit your changes locally with the following commit message "2.b Update README.md".
        8. Push your changes to remote.

        If everything went well you should get a merge conflict. Resolve it and make sure your local changes are stored on remote. Your git log should look something like this:
        ```sh
        b03a4c8 (HEAD -> main, origin/main, origin/HEAD) 2.c Update README.md (merge conflict)
        b8141ea 2.b Update README.md
        a17ad1b 2.a Update README.md
        cb23db4 1. Edit README.md
        ```
      ^merge: feature
      weight: 0.5

  - Simple website scraper:
    - 
      link: https://towardsdatascience.com/how-to-web-scrape-with-python-in-4-minutes-bc49186a8460
      title: How to Web Scrape with Python in 4 Minutes
      info: Web scraping is a technique to automatically access and extract large amounts of information from a website, which can save a huge amount of time and effort.
    -
      text: |
        On the following [site](https://towardsdatascience.com/how-to-web-scrape-with-python-in-4-minutes-bc49186a8460) an example is given on how to build a web scraper in Python. Your assignment is to create a scraper for the following website [https://news.ycombinator.com/](https://news.ycombinator.com/). Your app should fetch the data from the url using the requests library and list the number of articles from the website in your console (display the rank and title).

        Once finished commit and push your changes to the main branch.
      ^merge: feature

  - GitLab workflow - Timestamped CSV:
    -
      link: https://docs.gitlab.com/ee/user/project/merge_requests/
      title: Merge requests
      info: Merge requests (MRs) are the way you check source code changes into a branch. When you open a merge request, you can visualize and collaborate on the code changes before merge
    -
      text: |
        Committing changes on the main branch is not really a best practice when you are working with others in the same repository. The recommended way to add features to the main code base is by creating feature branches and merging them with the main branch when the feature has been fully implemented.

        Branching in Gitlab is usually done by opening a merge request. You can open a merge request on any branch you have already created. Note that after you have created the merge request nothing is merged yet. In Gitlab a 'conversation' is opened between you and the maintainer of the project's the main branch. In practice the maintainer will provide feedback on the code and possibly ask for things to be implemented differently.

        To better track the merge requests you can associate them with issues. Go to GitLab and create an issue for storing the articles in a CSV file (assign this issue to yourself). Adjust your code so that is creates a timestamped CSV file every time the application is run. Store the rank, title and link to the article in this file.

        Create a merge request for the issue and notice that a branch is created with the title of the issue. Locally checkout this branch and implement it. After you are finished push your code to remote and merge it with the main branch.
      ^merge: feature

  - Change some stuff on remote:
    -
      text: |
        At this point you should have merged the feature branch with the main branch (on remote). Switch locally to the main branch but do not pull in the new changes from remote just yet.

        Change the url in the code (locally) to [https://sd42.nl](https://sd42.nl). Commit the changes on the main branch.  Now do a 'git fetch'. What happens with your latest change? Now do a 'git pull'. What happens to your latest changes. Describe the difference in a file called 'notes.txt' and commit and push them on the main branch. Note: be sure that (on remote) the URL should be [https://news.ycombinator.com/](https://news.ycombinator.com/).

      ^merge: feature

  - Branch naming:
    -
      text: |
        Sometimes you can get naming conflicts when a branch has been created locally and a branch with the same name on remote. In this objective we will simulate this and you will be asked to solve this issue. 

        Before we start execute the following steps:
        1. On GitLab create a branch called 'feature_branch'.
        2. On this branch you should edit you Python code and change the URL to [https://www.tweakers.net](https://www.tweakers.net).
        3. Commit this change (on remote) and don't forget to give it a proper commit message. After the commit you will be asked if you want to create a merge request. Please do so.
        4. To set up our next objective please create a new issue in GitLab for unit testing. Also create a merge request for this issue. We will need this branch in our next objective.
        5. Now create a branch locally and call it 'feature_branch'. On this branch you should change you code for your scraper so that it fetches the modules from [https://sd42.nl/curriculum](https://sd42.nl/curriculum). Your scraper should output a list of names of all the modules in the curriculum.
        
        When finished commit and push. You should get an error that this is not possible. Resolve this issue and make sure that your changes are merged with the remote branch and that this branch is merged with main using the previously created merge request.

        Once merged describe how you have resolved this issue in the 'notes.txt' file. Commit and push this on the main branch. 
      ^merge: feature

  - Rebase:
    -
      link: https://www.w3docs.com/snippets/git/how-to-rebase-git-branch.html
      title: How to Rebase Git Branch
      info: A tutorial that shows how to rebase a remote branch on your feature branch.
    -
      text: |
        At this point you should have a branch from main which contains the code for scraping [https://news.ycombinator.com/](https://news.ycombinator.com/). Write a simple unit test that verifies the number of articles that have been fetched from the website (it should be 30). Once finished commit and push your changes.

        At this point your test should be successful but it has been implemented for [https://news.ycombinator.com/](https://news.ycombinator.com/). Meanwhile the main branch has been updated with a scraper for [https://sd42.nl/curriculum/](https://sd42.nl/curriculum/). You should now bring in these changes into your branch - using a rebase - and update your test so that it counts the number of modules. (This should be around 21 or 24, depending if you count stand-alone blocks like *Linux* as a module.)

        Once you have successfully updated you code merge the changes into main via the previously created merge request. 
      ^merge: feature

  - Commit the git log:
      must: true
      text: In order to see what your git history looks like please write the contents of your git log (--oneline) to a file called 'gitlog.txt'. Commit and push this to the main branch. Don't feel bad if your `git log` is a mess ;)
- |
    Today we are opening our Christmas tree shop. And we are selling some very strange trees. Let's build an application to showcase our three types of trees, of various heights. We'll be creating the application step-by-step, but here is how the final product should work:

    <script>
        renderAsciinema('trees.cast');
    </script>

- Tiny trees:
    ^merge: feature
    code: 0
    text: |
        Create the basics for the Christmas tree shop application shown above. For now, it won't have to ask for tree height, and it won't have to properly handle invalid user input.

        It should be able to produce output like this:

        ```
        *** Welcome to Santa's Woodshop ***

        Let's draw a tree!
        What shape should it have? [right/left/normal] right
        *
        **
        ***
        Would you like to see another tree? [yes/no] yes

        Let's draw a tree!
        What shape should it have? [right/left/normal] left
          *
         **
        ***
        Would you like to see another tree? [yes/no] yes

        Let's draw a tree!
        What shape should it have? [right/left/normal] normal
          *
         ***
        *****
        Would you like to see another tree? [yes/no] no
        ```

        *Hint*: You can create an 'infinite loop' by wrapping the repeatable part of your program in a `while True`. Use `break` to break out of a loop (making it not so infinite after all).

- Trees of all sizes:    
    ^merge: feature
    code: 0
    text: |
        Modify your program to ask for the tree height before your ask for the tree shape, and print trees of dynamic heights.

        Your application should now be able to produce output like this:

        ```
        *** Welcome to Santa's Woodshop ***

        Let's draw a tree!
        How tall would you like it to be? 5
        What shape should it have? [right/left/normal] right
        *
        **
        ***
        ****
        *****
        Would you like to see another tree? [yes/no] yes

        Let's draw a tree!
        How tall would you like it to be? 3
        What shape should it have? [right/left/normal] left
          *
         **
        ***
        Would you like to see another tree? [yes/no] yes

        Let's draw a tree!
        How tall would you like it to be? 10
        What shape should it have? [right/left/normal] normal
                 *
                ***
               *****
              *******
             *********
            ***********
           *************
          ***************
         *****************
        *******************
        Would you like to see another tree? [yes/no] no
        ```

        *Hint:* You'll need to add loops to go over each line of the trees. To repeat a string a certain number of times, you can multiply it by an integer. So `'test' * 3` would become `'testtesttest'`.

- Error handling:
    ^merge: feature
    code: 0
    text: |
        Finish your application, so that it works exactly like the video on the top of the assignment.

        - When it inputs an invalid tree height, it should ask again.
        - When it inputs an invalid tree shape, it should go back to `Let's draw a tree!`.
        - When it inputs an invalid yes/no choice, it should ask again.

        *Hint*: You'll need to add some loops, and you'll want to make use of `break` and `continue`.

-
    title: Punctuality
    must: true
    text: |
        Make sure that the output of your program:

        - Closely matches the provided video and example outputs.
        - Contains no spelling errors. *Hint*: Install the *Code Spell Checker* extension (by *streetsidesoftware*) for Visual Studio Code. Just take a minute to do this. Really.
        - Uses correct and easy to understand messages.
        - Uses capital letters, white spaces and punctuation in all the right places.

- Code quality:
    text: Your code should be easy to understand, using sensible variable names, empty lines to separate distinct parts of the program and consistent use of whitespace.
    ^merge: codequality
    weight: 1.5

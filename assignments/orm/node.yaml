name: Object Relational Mapping
goals:
    orm: 1
days: 2
depend: inheritance
pair: true
resources:
    -
        link: https://www.youtube.com/watch?v=dHQ-I7kr_SY
        title: Introduction to Object-Relational Mapping
        info: What is Object Relational Mapping? Why would you want to use it? This lecture talks about the general theoretical concept, and not about Python in specific. It has some drawings that may help you understand the concept. (You may want to increase the playback speed in parts somewhat. 😄)
    
    -
        link: https://www.youtube.com/watch?v=jTiyt6W1Qpo
        title: Pretty Printed - Getting Started With Flask-SQLAlchemy
        info: Learn how to setup Flask-SQLAlchemy, define a model, create the database, insert objects and retrieve those objects. Note that adding objects to a database based on an HTTP GET request is *not* a good idea in actual projects.

    -
        link: https://github.com/vanviegen/postgresqlite#using-flask-sqlalchemy
        title: PostgreSQLite - Using Flask-SQLAlchemy
        info: We've been using PostgreSQLite to manage the PostgreSQL database for us. This example code shows you how to use it together with SQLAlchemy.
    
    -
        link: https://www.youtube.com/watch?v=VVX7JIWx-ss
        title: Creating One-To-Many Relationships in Flask-SQLAlchemy
        info: Learn how to model one-to-many relationships using Flask-SQLAlchemy.

    -
        link: https://flask-sqlalchemy.palletsprojects.com/en/2.x/quickstart/
        title: Flask-SQLAlchemy Documentation - Quickstart
        info: This short quickstart guide covers about the same material as the two above videos. It's mostly just example code.

    -
        link: https://flask-sqlalchemy.palletsprojects.com/en/2.x/
        title: Flask-SQLAlchemy Documentation
        info: The rest of the official documentation. Highly recommended, as it has an index that lets you quickly find what you need and uses a lot of code examples.
assignment:
    SIMS 4000:
        - Let's modernize our renowned *SIMS 3000* invoice management system (from the *Python SQL* lesson) by bringing it to the web!
        -
            title: Flask-SQLAlchemy
            text: |
                Use only Flask-SQLAlchemy models to create and interact with the database.

                You'll need to create a new Poetry project, adding the various needed package. Additionally, we recommend installing the `flask-sqlalchemy-stubs`, `sqlalchemy-stubs` and `types-flask` Poetry packages, to get more relevant error messages in Visual Studio Code.

                Just for this once, you do *not* have to protect against CSRF attacks (although it sure wouldn't hurt!). We'll learn about an easier way to prevent CSRF next week.
            must: true
        -
            title: Main page
            ^merge: feature
            text: |
                <wired-card>
                    <h2>Main page</h2>
                    <div style="display: flex;">
                        <wired-card>
                            <h3>Customers</h3>
                            <ul>
                                <li><a href="#">Akyla</a></li>
                                <li><a href="#">Almexx</a></li>
                                <li><a href="#">Innovadis</a></li>
                                <li><a href="#">Label305</a></li>
                                <li><a href="#">Moneybird</a></li>
                                <li><a href="#">Takeaway.com</a></li>
                            </ul>
                            <wired-divider></wired-divider>
                            <div>Add a customer</div>
                            <wired-input placeholder="Customer name"></wired-input>
                            <wired-input type="submit" value="Add"></wired-button>
                        </wired-card>
                        <wired-card style="min-width: 200px;">
                            <h3>Unpaid invoices</h3>
                            <ul>
                                <li>
                                    <div style="font-size: 125%;">€750 </div>
                                    <a href="#">Fixed infinite-free-pizza bug</a>
                                    <div style="color: #666;">2021-01-15 Takeaway.com</div>
                                </li>
                                <li>
                                    <div style="font-size: 125%;">€3900 </div>
                                    <a href="#">Site rewrite in Flask</a>
                                    <div style="color: #666;">2021-01-21 Innovadis</div>
                                </li>
                            </ul>
                        </wired-card>
                    </div>
                </wired-card>
                
                The *Main page* contains...

                - A list of customers, showing just the name for each. It should be order alphabetically. Clicking a customer opens its *Customer page*.
                - A list of unpaid invoices (for any customer), showing the date, the description, the amount and the customer name for each. It should be ordered by date (oldest first). Clicking an invoice opens its *Invoice page*.
                - A *create customer form* that allows you to add a customer.
        -
            title: Customer page
            ^merge: feature
            text: |
                <wired-card>
                    <h2>
                        <a href="#" style="display: inline;">&lt;&lt;</a>
                        Customer: Innovadis
                    </h2>
                    <wired-card style="min-width: 200px;">
                        <h3>All invoices</h3>
                        <ul>
                            <li>
                                <div style="font-size: 125%;">€217999 </div>
                                <a href="#">Upgraded forward deflectors with unobtainium</a>
                                <div style="color: #666;">2021-01-15 Paid</div>
                            </li>
                            <li>
                                <div style="font-size: 125%;">€3900 </div>
                                <a href="#">Site rewrite in Flask</a>
                                <div style="color: #666;">2021-01-21 <span style="color: red;">Unpaid</span></div>
                            </li>
                        </ul>
                    </wired-card>
                    <div style="display: flex;">
                        <wired-card>
                            <div>New invoice</div>
                            <wired-input placeholder="Description"></wired-input>
                            <wired-input placeholder="Amount" type="numeric"></wired-input>
                            <wired-input type="submit" value="Create"></wired-button>
                        </wired-card>
                        <wired-card>
                            <div>Edit customer</div>
                            <wired-input placeholder="Name" value="Innovadis"></wired-input>
                            <wired-input type="submit" value="Save"></wired-button>
                        </wired-card>
                    </div>
                </wired-card>

                The *Customer page* contains...
                
                - A list of all invoices (paid and unpaid) for this customer, showing the date, the description, paid-status and the amount of each. The list should be ordered by amount (highest first). Clicking an invoice opens its *Invoice page*.
                - A *new invoice form* that allows you to create a new invoice for this customer. The date should be set automatically to the current date, and the status should be set to *unpaid*. The amount can be specified in whole EURs.
                - An *edit customer form* that allows you to change the customer's name.
                - A way to navigate back to the main page
        -
            title: Invoice page
            ^merge: feature
            text: |
                <wired-card>
                    <h2>
                        <a href="#" style="display: inline;">&lt;&lt;</a>
                        Invoice: Fixed infinite-free-pizza bug</h2>
                    </h2>
                    <div style="display: flex;">
                        <div>
                            <table>
                                <tr><td>Customer</td><td>Takeaway.com</td></tr>
                                <tr><td>Date</td><td>2021-01-15</td></tr>
                                <tr><td>Status</td><td style="color: red;">Unpaid</td></tr>
                            </table>
                            <wired-input type="submit" value="Mark as paid"></wired-button>
                        </div>
                        <wired-card>
                            <div>Edit invoice</div>
                            <wired-input placeholder="Description" value="Fixed infinite-free-pizza bug"></wired-input>
                            <wired-input placeholder="Amount" type="numeric" value="750"></wired-input>
                            <wired-input type="submit" value="Save"></wired-button>
                        </wired-card>
                    </div>
                </wired-card>

                The *Invoice page* contains...

                - An *edit invoice form* that allows you to change the invoice description and amount.
                - Either a *Mark as paid* or *Mark as unpaid* that toggles the invoice to the other state, and redirect to the *Customer page*.
                - A way to navigate back to the customer page

                <script>loadScript("/static/wired-elements.js")</script>

        -
            title: Layout and styling
            0: No styling whatsoever.
            1: Inconsistent layout.
            2: Acceptable layout.
            3: Consistent layout with appropriate and meaningful margins.
            4: Looks really great with consistent layout!
            text: |
                All pages should be consistently laid out (using a Jinja2 layout template) and styled to a degree acceptable for an internal administration tool. 
            
                **Tip:** Invest some time now to create a `.css` file that allows you to make any kind of form look good in little time. You can then reuse this file in future lessons and even during the exam, to save time.
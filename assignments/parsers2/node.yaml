name: Advanced parsers and compilers
description: Compile a high level language to SAX16 assembler!
goals:
    parsers: 1
    ast: 1
days: 2
depend: asmstack
about: |
    Successfully completing this assignment is a hard requirement for the exam, as you'll be asked to extend this work in the exam.
assignment:
    The Saxy language:
        - |
            Inventing programming languages is fun! That's why we're introducing our very own language: Saxy.

            To get a feel for the language, take a look at the `example?.saxy` files. In this language:

            - Supported types are strings and (16-bit unsigned) integers.
            - Integers act as fake booleans, where 0 means false and everything else means true.
            - The language is statically typed, meaning that a variable can hold only a single type (either string or integer). The type of the variable is determined at compile-time by the type of the assigned value of the first `set` to that variable.
            - Everything is an expression, meaning that language construct returns a value. There are no statements. 
                - Blocks of code can consist of multiple expressions separated by newline characters. The value of the last expression is the value for the block.
                - Similarly the `print!` built-in method returns the last of its arguments as its value.
            - The language has proper operator precedence, meaning that `1 * 2 + 3 * 4` is parsed mathematically correctly as `(1 * 2) + (3 * 4)` and not something like `1 * (2 + (3 * 4))` or `((1 * 2) + 3) * 4`.

        -
            ^merge: feature
            code: 0
            map:
                parsers: 1
            text: |
                Complete the production rules in `GRAMMAR.md`, based on the provided `example?.saxy` files and the parser output in the corresponding `.output` files.

                Note that we're *not* worrying about proper operator precedence for the binary operators just yet. We'll get to that later.

                Suggested time allocation: 1 hour.

    Tokenizer & parser:
    -
        ^merge: feature
        code: 0.2
        map:
            parsers: 2
        text: |
            Implement a tokenizer and a parser based on your `GRAMMAR.md`. Like in the previous assignment, we provided you with a small framework, but this time you also need to complete the tokenizer regular expression and we *haven't* provided you TODOs for all of the parser functions you'll need.

            Your parser should be able to create a reasonable parse tree (resembling the one in the `.output` files) for each of the 5 examples, except that the operator precedence for binary operators may still be incorrect.

            Suggested time allocation: 5 hours.
    -
        ^merge: feature
        code: 0.2
        map:
            parsers: 1
        text: |
            Make sure your parsers passes the tests provided in `test1.py`.

    Operator precedence:
    -
        link: https://craftinginterpreters.com/parsing-expressions.html#ambiguity-and-the-parsing-game
        title: Ambiguity and the Parsing Game
        info: Section 6.1 explains how you can add operator precedence to your grammar by *stratifying* your binary operator production rule. (You can stop reading at the *6.2 Recursive Descent Parsing* section - you already know that.)
    -
        ^merge: feature
        code: 0.1
        map:
            parsers: 1
        text: |
            Add proper operator precedence to your `GRAMMAR.md` and to your parser. These are the operators, from highest to lowest precedence:

            - `*` and `/`
            - `+` and `-`
            - `==` and `!=`

            Suggested time allocation: 2 hour.


    Saxy to SAX16:
    - |
        Let's compile/transpile Saxy to SAX16 assembler!

        When programming assembler, every tiny little thing you want to do involves quite a few lines of code. That's why we're providing `library.asm` for this assignment, which includes a number of useful functions and macros. The library can be used by assembling using `python asm.py library.asm <your_program>.asm`, which is done automatically by `main.py`. You won't need to make any changes to `library.asm`, but we recommend that you skim through it now, to see what's in there and to reacquaint yourself with SAX16 assembler.

        The library provides a couple of general purpose registers that you can freely use (`ra`, `rb` and `rc`). They are all 2 bytes in size, so that they can represent both Saxy types:

        - A 16 bit unsigned integer.
        - A string, by holding the memory address of the first character (byte) of the string. The end of the string is marked by a terminating 0 byte.        

        All production rules in Saxy should return their values in `*ra`, and can thus expect that when they have a child rule generate code, that code will cause its return value to be contained in `*ra`. We further recommend that code generated for production rules will not change the values for `*rb` and `*rc` (or restore them to their previous values using the stack).

    -
        ^merge: feature
        map:
            ast: 1
        code: 0.2
        text: |
            Get all of the `example?.saxy` programs to assemble and execute as shown in the corresponding `.output` files by finishing the `transpile()` methods in `saxy_tree.py`.

            But before you do that, we suggest that you carefully study the code that is already there in `saxy_tree.py`. Some of the `transpile()` methods have been provided as working examples. All of the AST node classes also have a working `run()` method, which provide Saxy with a way to run as an interpreted language as well as a compiled language. Also, the `run()` methods may help you with figuring out what the assembler code generated by the `transpile()` should actually do.

            *Note:* though some basic knowledge of SAX16 certainly helps for this objective, you won't need to come up with any assembler code yourself, as you can peek in the `.output` files.

            Suggested time allocation: 1 day.

    -
        ^merge: feature
        code: 0
        map:
            ast: 0.5
        text: |
            Make sure your transpiler passes the tests provided in `test2.py`.

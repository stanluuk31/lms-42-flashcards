import sys
import traceback
import json
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import NoReturn


def print_tree(data, tab=''):
    """Display an indented view of the AST."""
    next_tab = tab+'    '
    if isinstance(data, str | int | float | bool) or data is None:
        print(repr(data))
    elif isinstance(data, list):
        print('[')
        for item in data:
            print(next_tab, end='')
            print_tree(item, next_tab)
        print(tab + ']')
    else:
        print(data.__class__.__name__ + ' {')
        attrs = {attr: getattr(data, attr) for attr in dir(data) if not callable(getattr(data, attr)) and not attr.startswith("_")}
        for key, value in attrs.items():
            print(f'{next_tab}{key}: ', end='')
            print_tree(value, next_tab)
        print(tab+'}')


def error(message) -> NoReturn:
    """Display an error message and exit the application with an error code."""
    traceback.print_stack(file=sys.stdout)
    print(f"\nERROR: {message}.", file=sys.stderr)
    sys.exit(1)


class RunData:
    """Class used for maintaining run-time data (variable values) when running (interpreting) the program."""
    def __init__(self):
        self.variables: dict[str, int | str] = {}


class TranspileData:
    """Class used for maintaining compile-time data when transpiling the program."""
    def __init__(self):
        self.section_outputs = {
            'variables': '',
            'literal_strings': '',
            'code': '',
            'heap': '',
        }
        self.variable_types: dict[str, str] = {}
        self.label_counts = {}

    def add(self, code: str, comment: str | None = None, section: str = "code"):
        """Add assembly code to (a certain section of) the output file."""
        if comment:
            code += " " * max(1, 30-len(code))
            code += "# " + comment
        self.section_outputs[section] += code + "\n"

    def get_unique_label(self, prefix):
        """Make a string unique by suffixing a number to it."""
        if prefix not in self.label_counts:
            self.label_counts[prefix] = 0
        self.label_counts[prefix] += 1
        return f"{prefix}{self.label_counts[prefix]}"

    def make_assembly(self):
        """Gather the sections created using `add` statements into a single string."""
        output = ''
        for name, text in self.section_outputs.items():
            comment_name = ' ' + name.replace('_', ' ').capitalize() + ' '
            output += f"#####{comment_name:{'#'}<75}\n{text}\n\n"
        return output


class Expression(ABC):
    """All tree nodes inherit from this abstract class, and implement its two methods."""
    @abstractmethod
    def run(self, data: RunData) -> int | str:
        """Should execute the instruction and return its value."""

    @abstractmethod
    def transpile(self, data: TranspileData) -> str:
        """Should generate code to the `data` object and return the expression type as a string ("int" or "str")."""


@dataclass
class Block(Expression):
    """A list of expressions to be executed sequentially, returning the value of the last expression."""
    expressions: list[Expression]

    def run(self, data: RunData) -> str | int:
        value = 0
        for expression in self.expressions:
            value = expression.run(data)
        return value

    def transpile(self, data: TranspileData) -> str:
        expression_type = "int"
        for expression in self.expressions:
            expression_type = expression.transpile(data)
            # The return value set by the last expression (in *ra)
            # will be our return value.
        return expression_type


@dataclass
class Program(Expression):
    """The top-level expression. Should not be nested."""
    block: Block

    def run(self, data: RunData):
        return self.block.run(data)

    def transpile(self, data: TranspileData):
        data.add('main:', 'execution starts here')
        self.block.transpile(data)
        data.add('jump 0', 'halt program')

        data.add('heap:', section='heap')
        return 'int'


@dataclass
class Assignment(Expression):
    """Create or update a variable, returning the new value."""
    identifier: str
    expression: Expression

    def run(self, data: RunData):
        value = self.expression.run(data)
        data.variables[self.identifier] = value
        return value

    def transpile(self, data: TranspileData):
        # Have self.expression generate code to put a value in *ra. (The method will return the data type.)
        var_type = self.expression.transpile(data)

        if self.identifier not in data.variable_types:
            # If the variable doesn't exist yet, remember the variable type.
            data.variable_types[self.identifier] = var_type
            # And allocate space for the variable in the 'variables' section
            data.add(f"var_{self.identifier}: int16 0", "allocate space for variable", section="variables")
        elif var_type != data.variable_types[self.identifier]:
            # Variable already exists. Check if the type matches.
            error(f"Variable '{self.identifier}' cannot change type")

        # Save the file in *ra to the space allocated for the variable
        data.add(f"set16 *var_{self.identifier} *ra", "set variable")

        # Return the variable type.
        return var_type


@dataclass
class Literal(Expression):
    """A literal string or integer."""
    value: int | str

    def run(self, data: RunData):  # noqa: ARG002
        return self.value

    def transpile(self, data: TranspileData):
        if isinstance(self.value, str):
            label = data.get_unique_label("literal_string")
            # (Ab)use json encoding to add quotes and escape special characters
            string = json.dumps(self.value)
            data.add(f'{label}:', section="literal_strings")
            data.add(f'{string}', section="literal_strings")
            data.add('int8 0', comment="terminate the string with a zero byte", section="literal_strings")
            data.add(f"set16 *ra {label}")
            return 'str'
        else:  # noqa: RET505 (self.value is an int)
            data.add(f"set16 *ra {self.value}")
            return 'int'


@dataclass
class BinaryOperator(Expression):
    left: Expression
    operator: str
    right: Expression

    def run(self, data: RunData) -> str | int:  # noqa: C901, PLR0911
        left_result = self.left.run(data)
        right_result = self.right.run(data)

        op = self.operator
        if isinstance(left_result, int) and isinstance(right_result, int):
            if op == '+':
                return left_result + right_result
            if op == '==':
                return 1 if left_result == right_result else 0
            if op == '!=':
                return 1 if left_result != right_result else 0
            if op == '-':
                return left_result - right_result
            if op == '*':
                return left_result * right_result
            if op == '/':
                return left_result // right_result
            
        elif isinstance(left_result, str) and isinstance(right_result, str):
            if op == '+':
                return left_result + right_result
            if op == '==':
                return 1 if left_result == right_result else 0
        else:
            error(f"Types differ in binary operation {self.operator}")

        return error(f"Unknown operator '{op}'")

    def transpile(self, data) -> str:
        # Save *rb on the stack
        data.add("push *rb", f"make *rb safe for use by binary '{self.operator}' operator")

        # Evaluate the right operand and put it in *rb.
        right_type = self.right.transpile(data)
        data.add("set16 *rb *ra", f"*rb contains right '{self.operator}' operand")

        # Evaluate the left operand and put it in *ra.
        left_type = self.left.transpile(data)
        data.add("", f"*ra contains left '{self.operator}' operand")

        if left_type != right_type:
            error(f"Types differ in binary operation {self.operator}")

        return_type = left_type
        op = self.operator
        if left_type == "int":
            # Generate code for integer binary operators.
            if op == '+':
                data.add("add16 *ra *rb")
            elif op == '==':
                equal_label = data.get_unique_label("ints_equal")
                equal_end_label = data.get_unique_label("ints_equal")
                data.add(f"if=16 *ra *rb jump {equal_label}")
                data.add("set16 *ra 0", "Unequal")
                data.add(f"jump {equal_end_label}")
                data.add(f"{equal_label}:")
                data.add("set16 *ra 1", "Equal")
                data.add(f"{equal_end_label}:")
            # TODO: Add additional operators.
            else:
                error(f"Binary '{self.operator}' operator not defined for int values")

        else:
            # Generate code for string binary operators.
            assert left_type == "str"
            if op == '+':
                data.add("call concat_strs")
            elif op == '==':
                data.add("call compare_strs")
                return_type = 'int'
            else:
                error(f"Binary '{self.operator}' operator not defined for str values")
                
        # Restore *rb.
        data.add("pop *rb")

        return return_type


@dataclass
class IfExpression(Expression):
    condition: Expression
    then: Expression
    otherwise: Expression

    def run(self, data: RunData) -> str | int:
        if self.condition.run(data) != 0:
            return self.then.run(data)
        return self.otherwise.run(data)
            
    def transpile(self, data: TranspileData) -> str: # noqa: PLR6301, ARG002
        # TODO. Hints:
        # - Use data.get_unique_label("your_identifier") to generate labels to use in assembler.
        # - Look through the `.output` files to find inspiration on what assembler should be generated.
        # - Look through the other transpile() methods for ideas on how to generate assembler code.
        # - Both the then-expression and the otherwise-expression should return the same type. If not, call `error()`.
        # - Don't forget to have this method return 'str' or 'int'.
        return "IMPLEMENT ME"


@dataclass
class LoopExpression(Expression):
    """Loops at least once, repeating until the condition returns 0, and returning the
    value of the last do-expression execution."""
    do: Expression
    condition: Expression

    def run(self, data: RunData) -> str | int:
        while True:
            result = self.do.run(data)
            if self.condition.run(data) == 0:
                return result
            
    def transpile(self, data: TranspileData) -> str: # noqa: PLR6301, ARG002
        # TODO
        return "IMPLEMENT ME"


@dataclass
class VariableReference(Expression):
    """Get the value of the variable by the given name (which should have been created by Assignment)."""
    name: str

    def run(self, data: RunData) -> str | int:
        if self.name not in data.variables:
            error(f"Variable '{self.name}' is not declared")
        return data.variables[self.name]

    def transpile(self, data: TranspileData) -> str: # noqa: PLR6301, ARG002
        # TODO. Hint: look into the Assignment.transpile() method. Also, don't forget to return the value type.
        return "IMPLEMENT ME"


@dataclass
class InputOutput(Expression):
    """Read user input (`str?` and `int`?) or print output (`print!`). These three
    methods have in common that all argument expressions are printed (first)."""
    name: str
    expressions: list[Expression]

    def run(self, data: RunData):
        # Print
        value = 0
        for expr in self.expressions:
            value = expr.run(data) # either a str or an int
            print(value, end="")
        # Optional read
        if self.name == 'str?':
            return input()
        if self.name == 'int?':
            try:
                return int(input())
            except ValueError:
                return 0
        assert self.name == 'print!'
        # Return the last printed value
        return value
        
    def transpile(self, data: TranspileData) -> str: # noqa: PLR6301, ARG002
        # TODO. Hints:
        # - Use the `print_str`, `print_int`, `read_str` and `read_int`
        #   functions defined in `library.asm`.
        # - Return the last printed value.
        return "IMPLEMENT ME"


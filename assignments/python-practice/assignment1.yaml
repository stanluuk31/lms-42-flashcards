- |
    Create a terminal application in which you and other players can play a game of 'mens erger je niet'.
    <img src="mens-erger-je-niet.jpg" class="no-border">

-   Features:
    -   Startup the game:
            ^merge: feature
            code: 0
            map:
                termio: 1
                loops: 1
                vars: 1
                lists: 1
                strings: 1
                exceptions: 1
            text: |
                When starting your application it should...
                - Ask for the number of players with a minimum of 2 and a maximum of 4, retrying if something else was entered.
                - Ask for each player what color he or she wishes to be.
                - Check whether the provided color corresponds to a color in the provided list.
                - Prevent different players from selecting the same color.

                Example output:

                ```
                *** Mens Erger Je Niet ***
                With how many players would you like to play [2-4]: 2
                Choose a color for player #1 [red, green, yellow, black]: red
                Choose a color for player #2 [green, yellow, black]: orange
                Invalid color!
                Choose a color for player #2 [green, yellow, black]: red
                Invalid color!
                Choose a color for player #2 [green, yellow, black]: green

                ```

    - Simple game representation:
            ^merge: feature
            code: 0
            map:
              termio: 1 
              dicts: 1
            text: |
                Create a function that displays the game state. It should be called before each turn and at the end of the game.
                
                At the start of the game, the state should be displayed like this:
                ```
                Players:
                   0. red (starting square: 0, pawns available: 4, pawns home: 0)
                   1. green (starting square: 10, pawns available: 4, pawns home: 0)
                ```

                Here's what the fields mean:

                - *starting square* is the position on the playing board where pawns will first appear when 6 is thrown. This should be 0, 10, 20, 30 for players 0, 1, 2 and 3 respectively.
                - *pawns available* is the number of a player's pawns that are not actively in the game yet. A pawn becomes active when a player throws 6.
                - *pawns home* is the number of a player's pawns that have successfully walked around the game board, and have arrived home safely. The first player to get all pawns home wins.

                You should store all this information in suitable variables.

                When a player has pawns on the board, these pawns are displayed ordered by square number. The complete state output might look like this:
                ```
                Players:
                   0. red (starting square: 0, pawns available: 2, pawns home: 0)
                   1. green (starting square: 10, pawns available: 2, pawns home: 0)
                Board:
                   1: red
                  10: green
                  17: red
                  36: green
                ```

    - Playing a turn:
            ^merge: feature
            code: 0
            map:
                flow: 1
                loops: 1
                functions: 1
                lists: 1
                modules: 1
            text: |
                For each turn to be played, your program should:

                - Show the color of the player who's turn it is.
                - Ask the user to press a key to roll the dice.
                - Show the dice value.
                - Act on the dice value:
                    - When a player rolls 6 *and* has pawns available, place a pawn on the player's starting square (explained in the previous objective), removing it from the *available* pawns.
                    - Otherwise, a pawn should be selected to move:
                        - If the player has no active pawns on the board (so that's excluding pawns that are *available* or *home*), no pawn can be selected, and the turn is skipped.
                        - If the player has only one active pawn on the board, that pawn will be selected automatically.
                        - If the player has more than one, she/he gets to choose which pawn to move.
                    - The selected pawn will move the number of positions forward indicated by the dice. After the last (40th) square one the board, it should move to the first square.
                    - In case the pawn lands on a square where another pawn used to be, that other pawn is removed from the board and sent back to the *available* pawns. This even happens when a pawn lands on a pawn of the same color.

                Some example turns:

                ```
                Player green:
                  Press enter to roll the dice...                                   
                  You rolled: 4!                                                
                  No moveable pawns.       
                ```

                ```
                Player red:
                  Press enter to roll the dice... 
                  You rolled: 6!
                  Placing a pawn on the starting square!
                ```

                ```
                Player black:
                  Press enter to roll the dice... 
                  You rolled: 2!
                  Pawn moving from 0 to 2.
                ```

                ```
                Player yellow:
                  Press enter to roll the dice... 
                  You rolled: 1!
                  The pawn at which square would you like to move? (17, 0) 0
                  Pawn moving from 0 to 1.
                ```
                  
                *Hint:* For easier testing, implement cheating. Design the roll dice function in such a way that you can modify the value yourself if you fill in a number instead of pressing `enter`. 

    - Pawns arriving home:
            ^merge: feature
            code: 0
            map:
                flow: 1
                functions: 1
            text: |
                When a pawns has walked around the entire game board, and would arrive *exactly* at the player's starting square, the pawn should be taken from the game board, and marked as *home*. It is now safe, and can no longer be removed from the board by being stepped on. (So contrary to the rules you may know, pawns don't need to *fill* the *home* area, they just need to land at the exact starting square.)

                In case the pawn would move *over* the starting square (a too high number was thrown), this should be explained to the user and the turn should be skipped. (This is surprisingly tricky to get right - ignore this part if it takes you too much time.)

                When all four of a player's pawns are home, that player is declared the winner and the game should end.

                ```
                Players:
                   0. red (starting square: 0, pawns available: 2, pawns home: 1)
                   1. green (starting square: 10, pawns available: 0, pawns home: 0)
                Board:
                   9: green
                  10: green
                  12: green
                  27: red
                  31: green

                Player green:
                  Press enter to roll the dice... 
                  You rolled: 2!
                  The pawn at which square would you like to move? (9, 31, 12, 10) 9
                  Pawn would go past home.
                ```

                Later on:

                ```
                Player green:
                  Press enter to roll the dice... 
                  You rolled: 1!
                  The pawn at which square would you like to move? (9, 35, 14, 10) 9
                  Pawn has arrived home!
                ```
    
    - Preserve game state:
            ^merge: feature
            code: 0
            map:
                fileio: 1
                modules: 1
                strings: 1
                loops: 1
            text: |
                After every turn the game state should be saved in a file. Whenever the user starts the application a check should be made whether there is a file present containing the state of the game. If so then the file should be read and the state of the game should be restored so that the players can pick up where they left. At the end of the game (when a winner has been determined) the file containing the game state should be removed.

                The game state file should look like this:

                ```
                green/a/h/15/30
                red/a/a/a/5
                ```

                This would mean that `green` is the first player (at starting square 0), and has one *available* pawn, one *pawn* that is home, a pawn at square 15 and a pawn at square 30. Player `red` (at starting square 10) has three available pawns, and one pawn at square 5. The order of the pawns doesn't matter.

                Write your code related to storing and loading the game state in a separate module.
    
    - Advanced game representation:
            ^merge: feature
            code: 0
            map: 
                debugging: 1
                modules: 1
                strings: 1
                exceptions: 1
            text: |
                Looks matter, when it comes to games! Let's show the state of the game in a way that resembles an actual game board, as shown in the photo at the top of this page.

                The good news is that this has already been implemented for you in the `board_printer` module. The bad news: you'll have to integrate it with your code, and it might contain some small bugs (which you should always read as: "it's buggy as hell!").

                Get it to work! Replace the *Simple game representation* with the more advanced one provided by `board_printer` in such a way that one can easily switch between the two implementations (for instance based on a constant variable).

                The end result should look like the following examples.

                - Initial game state with three players:

                ```
                                           20
                   |-----------------------------------------|
                   |                          r  r           |
                   |                [ ][ ][ ] r  r           |
                   |                [ ] _ [ ]                |
                   |  b  b          [ ] _ [ ]                |
                   |  b  b          [ ] _ [ ]                |
                10 |    [ ][ ][ ][ ][ ] _ [ ][ ][ ][ ][ ]    |
                   |    [ ] _  _  _  _                [ ]    |
                   |    [ ][ ][ ][ ][ ] _ [ ][ ][ ][ ][ ]    | 30
                   |                [ ] _ [ ]                |
                   |                [ ] _ [ ]                |
                   |                [ ] _ [ ]                |
                   |           y  y [ ][ ][ ]                |
                   |           y  y                          |
                   |-----------------------------------------|
                                    0
                ```


                - After some time the board might look like this. Black has one pawn at home, and one still available. Yellow also has one pawn home. Red appears to be losing.

                ```
                                           20
                   |-----------------------------------------|
                   |                          r  _           |
                   |                [ ][ ][ ] r  r           |
                   |                [ ] _ [b]                |
                   |  _  _          [ ] _ [ ]                |
                   |  _  b          [ ] _ [ ]                |
                10 |    [b][y][ ][ ][ ] _ [ ][ ][ ][ ][ ]    |
                   |    [ ] b  _  _  _                [ ]    |
                   |    [ ][ ][ ][ ][ ] _ [ ][ ][ ][ ][ ]    | 30
                   |                [ ] _ [r]                |
                   |                [ ] _ [ ]                |
                   |                [ ] y [ ]                |
                   |           _  y [ ][ ][ ]                |
                   |           _  y                          |
                   |-----------------------------------------|
                                    0
                ```

    -   Code quality:
            ^merge: codequality
            malus: 0
            bonus: 0
            map:
                comments: 1
                naming: 1

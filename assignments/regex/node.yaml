name: Regular Expressions
description: Learn about the swiss army knife for working with strings.
goals:
    regex: 1
resources:
    -
        link: https://github.com/ziishaned/learn-regex/blob/master/README.md
        title: Learn Regex the Easy Way
        info: A document without too much text and with just enough brief examples to learn most of what you need to know. It also has links to an interactive playground for all examples. Experiment, to make sure you understand!
    -
        link: https://regexone.com
        title: Learn Regular Expressions with simple, interactive exercises.
        info: Before moving on, we recommend that you work your way through this interactive tutorial.
    -
        link: https://www.tutorialspoint.com/regular-expression-in-python-with-examples
        title: Regular Expression in Python with examples
        info: How to use regular expressions from within your Python code.
    -
        link: https://www.tutorialspoint.com/What-is-Raw-String-Notation-in-Python-regular-expression
        title: What is Raw String Notation in Python regular expression?
        info: Make sure you use raw-strings when writing creating a regex in Python.
    -
        link: https://www.geeksforgeeks.org/verbose-in-python-regex
        title: Verbose in Python Regex
        info: How to make your regular expressions readable. 
    -
        link: https://www.dataquest.io/wp-content/uploads/2019/03/python-regular-expressions-cheat-sheet.pdf
        title: Data Science Cheat Sheet - Python Regular Expressions
        info: A brief reference on a single A4.
    -
        link: https://docs.python.org/3/library/re.html
        title: Python Documentation - Regular Expression operations
        info: The official documentation. Search here if you need details!
    -
        link: https://regex101.com
        title: Build, test and debug regex.
        info: When debugging a regex, this can be a lifesaver!
    - |
        For when you mastered regex and want to have some fun.
    -
        link: https://regexcrossword.com
        title: A crossword puzzle game using regular expressions.
        info: If you hate your live, you can sit back and relax and do regex puzzles!
    -
        link: https://xkcd.com/208/
        title: Regular expressions.
        info:  Of course there is a relevant xkcd.
        
    

assignment:
    Primer:
    -
        must: true
        text: |
            To get some hands-on experience with regular expression before starting work on the actual assignment, please fulfill the `TODO`s in `primer.py`.

    Assignment: |
        Your assignment is to implement just a single regular expression. That doesn't sound to bad, right? ... Right?!

        The regular expression should provide the basis for a syntax highlighter for Python code. The rest of the syntax highlighter is already provided in `highlighter.py` (which consists of surprisingly little code). Your regular expression should, given a Python string...

        - Match recognized parts of the input within *named capture groups*. So the regex should return a number (like `-12.34`) as a single match, and have it contained in a capture group named `number`. There should be capture groups for `number`, `keyword`, `identifier`. `comment` and `string`.
        - Match *everything*. So every character in every possible input string should be matched when `find_all` is used on the regex. Unrecognized parts should be returned as single characters without a capture group. (Note: this should include `\n` new line character, which `.` doesn't match!)

    Example matches: |
        So, for example the following input..

        ```python
        test = -23.4
        # test 1, 2, 3
        ```

        should have the following matches:

        | Match | Capture group name |
        | ----- | ------------------ |
        | `test` | identifier |
        | (space) | |
        | `=` | |
        | (space) | |
        | `-23.4` | number |
        | `\n` | |
        | `# test 1, 2, 3` | comment |


    Example highlighting: |
        If you manage to perfectly implement the regex in `highlighter.py`, the output of running
        
        ```sh
        poetry install && poetry run python highlighter.py example.py
        ```
        
        should look like this:

        <img src="example-highlighted.png" style="border: 8px solid black;">

    Warning: |
        Don't just copy regex gibberish from the internet. You need to be able to explain every character in your regex! 

    Match objectives:
        -
            ^merge: feature
            code: 0
            title: Numbers
            text: Highlight decimal numbers, including the optional leading minus sign (-123) and the fractional part (345.567).
        -
            ^merge: feature
            code: 0
            title: Keywords
            text: Highlight all [Python keywords](https://docs.python.org/3/library/keyword.html). They should of course not be highlighted when they're part of a larger word. So `in` should be highlighted as a keyword, but the *in* in `info` should not.
        -
            ^merge: feature
            code: 0
            title: Identifiers
            text: All identifiers (like variables names, class names, package names, etc) that are not keywords should be highlighted as identifiers.
        -
            ^merge: feature
            code: 0
            title: Comments
            text: Comments (starting with a `#` and running until the end of the line) should be highlighted as such. Of course, this should not happen when the `#` is part of a string.
        -
            ^merge: feature
            code: 0
            title: Strings
            weight: 2
            text: Highlight `"double quoted"`, `'single quoted'`, `"""double quoted multi-line"""` and `'''single quoted multi-line'''` strings. The single line variants should allow for `"backslash escaped \"quotes\""`. An optional `r` or `f` prefix (for raw and formatted strings) should also be highlighted as part of the string.
        -
            ^merge: feature
            code: 0
            title: The rest
            text: Everything else (mostly characters such as `=`, `>` and `(`) should not be highlighted. It's displayed in the default color (white).

    Regex readability:
        weight: 2
        ^merge: codequality
        text: |
            Although regular expressions can be notoriously hard to read, they don't have the be when using Python. Use *verbose mode* to make your regular expression (relatively) easy to read.

    But why?:
        weight: 2
        2: A bit vague, but definitely thought in the right direction.
        4: Solid understanding, well explained, or a working implementation with separate regexes.
        text: |
            Explain (in `but_why.txt`) why it makes sense to match all syntax features (keyword, identifier, etc) in a single regular expression, instead of one for each? 

            If you don't see the reason yet, make a copy of your working `highlighter.py` and see if you can do it with separate regular expressions. If you manage to get it to work (which *is* possible, with some extra work), that's also more than enough proof that you understand (so you can leave `but_why.txt` blank).


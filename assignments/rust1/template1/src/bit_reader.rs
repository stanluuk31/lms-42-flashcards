pub struct BitReader {
    /// The input data array
    bytes: Vec<u8>,
    /// The next byte to read from
    byte_offset: usize, 
    // The next bit in that byte to read from
    bit_offset: usize, 
}


impl BitReader {
    pub fn new(bytes: Vec<u8>) -> BitReader {
        // TODO: return a new BitReader, with correctly initialized values.
    }

    /// Returns 0 or 1 and moves on to the next bit.
    /// For each byte, the most significant bit is returned first.
    pub fn get_bit(&mut self) -> u8 {
        0 // TODO!
    }

    /// Reads `bit_count` bits and returns a (optionally signed) number from it.
    /// Like get_bit(), the most significant bits are read first.
    pub fn get_number(&mut self, bit_count: u8, signed: bool) -> i64 {
        0 // TODO!
    }
}


// Although we haven't explored Rust unit tests yet, the tests below should 
// look very familiar. Run them using: `cargo test`

#[cfg(test)]
mod tests {
    use crate::bit_reader::BitReader;

    #[test]
    fn test_bit() {
        let mut br = BitReader::new(Vec::from([0x0f, 0xf0]));
        for bit in 0..4 {
            assert_eq!(br.get_bit(), 0, "Bit {} in sequence 1", bit);
        }
        for bit in 0..8 {
            assert_eq!(br.get_bit(), 1, "Bit {} in sequence 2", bit);
        }
        for bit in 0..4 {
            assert_eq!(br.get_bit(), 0, "Bit {} in sequence 3", bit);
        }
    }

    #[test]
    fn test_unsigned() {
        let mut br = BitReader::new(Vec::from([0x12, 0x34, 0x56]));
        assert_eq!(br.get_number(4, false), 0x1);
        assert_eq!(br.get_number(8, false), 0x23);
        assert_eq!(br.get_number(12, false), 0x456);
    }

    #[test]
    fn test_signed() {
        let mut br = BitReader::new(Vec::from([0x80, 0xff, 0x00, 0x7f]));
        assert_eq!(br.get_number(8, true), -128);
        assert_eq!(br.get_number(8, true), -1);
        assert_eq!(br.get_number(8, true), 0);
        assert_eq!(br.get_number(8, true), 127);
    }

    #[test]
    #[should_panic]
    fn test_panic() {
        let mut br = BitReader::new(Vec::from([0x12, 0x34, 0x56]));
        assert_eq!(br.get_number(24, false), 0x123456);
        br.get_number(1, false);
    }
}

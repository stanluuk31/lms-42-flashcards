import datetime
import math
import sqlalchemy as sa
import re
import glob
import os
import openai
import traceback

from lms42.models.attempt import Attempt
from ..app import db
from ..utils import unsafe_markdown_to_html
from ..assignment import Assignment
from multiprocessing import Process, Queue
import json


class AiQuery(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'))
    attempt = sa.orm.relationship("Attempt")

    request_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    request_user = sa.orm.relationship("User")

    question = sa.Column(sa.String, nullable=False)
    answer = sa.Column(sa.String, nullable=False)
    prompt = sa.Column(sa.String, nullable=False)

    milli_dollars = sa.Column(sa.Integer, nullable=False, default=lambda context: get_price(context.current_parameters['model'], context.current_parameters['prompt_tokens'], context.current_parameters['completion_tokens']))

    model = sa.Column(sa.String, nullable=False)
    prompt_tokens = sa.Column(sa.Integer, nullable=False)
    completion_tokens = sa.Column(sa.Integer, nullable=False)

    def to_html(self):
        return "<article><div class=question>" + unsafe_markdown_to_html(self.question) + "</div><div class=answer>" + unsafe_markdown_to_html(self.answer) + "</div></article>"


def get_price(model: str, prompt_tokens: int, completion_tokens: int): # milli-dollars per thousand tokens
    if model.startswith("gpt-3.5-turbo"):
        in_price, out_price = 0.5, 1.5
    elif model.startswith("gpt-4o"):
        in_price, out_price = 5, 15
    elif model.startswith('gpt-4-turbo') or model in {'gpt-4-1106-preview', 'gpt-4-0125-preview'}:
        in_price, out_price = 10, 30
    elif model.startswith("gpt-4-32k"):
        in_price, out_price = 60, 120
    elif model.startswith("gpt-4"):
        in_price, out_price = 30, 60
    elif model == 'claude-2.1':
        in_price, out_price = 8, 24
    else:
        raise ValueError(f"Unknown model price for {model}")

    return math.ceil(in_price * prompt_tokens / 1000 + out_price * completion_tokens / 1000)


class AiGradingPrompter(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)
    python_code = sa.Column(sa.String, nullable=False)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    user = sa.orm.relationship("User")


class AiGrading(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    grading_prompter_id = sa.Column(sa.Integer, sa.ForeignKey('ai_grading_prompter.id'), nullable=True)
    grading_prompter = sa.orm.relationship("AiGradingPrompter")

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'), nullable=True)
    attempt = sa.orm.relationship("Attempt")

    input = sa.Column(sa.String, nullable=False)
    prompt = sa.Column(sa.String, nullable=False)
    output = sa.Column(sa.String, nullable=False)

    __table_args__ = (
        sa.Index('ai_grading_attempt_index', 'attempt_id'),
    )

    @property
    def output_object(self):
        try:
            return json.loads(self.output)
        except json.JSONDecodeError:
            return {'invalid_json': self.output}

    @property
    def prompt_object(self):
        try:
            return json.loads(self.prompt)
        except json.JSONDecodeError:
            return {'invalid_json': self.prompt}

    @property
    def milli_dollars(self):
        if self.model.startswith("gpt-3.5-turbo-16k"):
            in_price, out_price = 3, 4
        elif self.model.startswith("gpt-3.5-turbo"):
            in_price, out_price = 1, 2
        elif self.model == 'gpt-4-1106-preview':
            in_price, out_price = 10, 30
        elif self.model == 'claude-2.1':
            in_price, out_price = 8, 24
        elif self.model.startswith("gpt-4-32k"):
            in_price, out_price = 60, 120
        elif self.model.startswith("gpt-4"):
            in_price, out_price = 30, 60
        else:
            raise ValueError(f"Unknown model price for {self.modal}")

    return math.ceil(in_price * prompt_tokens / 1000 + out_price * completion_tokens / 1000)


class AiGradingPrompter(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)
    python_code = sa.Column(sa.String, nullable=False)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    user = sa.orm.relationship("User")


class AiGrading(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    grading_prompter_id = sa.Column(sa.Integer, sa.ForeignKey('ai_grading_prompter.id'), nullable=True)
    grading_prompter = sa.orm.relationship("AiGradingPrompter")

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'), nullable=True)
    attempt = sa.orm.relationship("Attempt")

    input = sa.Column(sa.String, nullable=False)
    prompt = sa.Column(sa.String, nullable=False)
    output = sa.Column(sa.String, nullable=False)

    __table_args__ = (
        sa.Index('ai_grading_attempt_index', 'attempt_id'),
    )

    @property
    def output_object(self):
        try:
            return json.loads(self.output)
        except json.JSONDecodeError:
            return {'invalid_json': self.output}

    @property
    def prompt_object(self):
        try:
            return json.loads(self.prompt)
        except json.JSONDecodeError:
            return {'invalid_json': self.prompt}

    @property
    def input_object(self):
        try:
            return json.loads(self.input)
        except json.JSONDecodeError:
            return {'invalid_json': self.input}


def read_files(attempt, subdir):
    pre = f"{attempt.directory}/{subdir}/"
    for filename in glob.glob(pre+"**", recursive=True):
        if not os.path.isfile(filename):
            continue
        assert filename.startswith(pre)
        short = filename[len(pre):]
        if '/data/' in filename or '/.' in filename or '/_' in filename or filename.split('.')[-1].lower() not in {'py', 'java', 'md', 'rs', 'js', 'sql'}:
            continue

        with open(filename) as file:
            try:
                yield short, file.read()
            except UnicodeDecodeError:
                # The file is likely encrypted
                yield short, "This file cannot be read."


def examine_attempt(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    ao = Assignment.load_from_directory(attempt.directory)
    assignment = ao.to_ai_markdown().strip()
    assignment = re.subn(r'(?s)<script.*?</script>', '', assignment)[0]
    assignment = re.subn(r'(?s)<link.*?>', '', assignment)[0]
    input_data: dict = {
        'assignment': assignment,
        **ao.node
    }
    for k in ('in_progress_topics', 'format_version', 'period', 'module_index',):
        input_data.pop(k, None)

    for file_type in ('template', 'solution', 'submission'):
        input_data[file_type] = {}
        for short, content in read_files(attempt, file_type):
            input_data[file_type][short] = content

    prompter = AiGradingPrompter.query.order_by(AiGradingPrompter.id.desc()).limit(1).first()
    if not prompter:
        # No prompter stored
        return False

    print(f'Running prompt version {prompter.id}')

    prompt = run_in_sandbox(prompter.python_code, "make_prompt", input_data)
    if not prompt:
        return False

    if 'error' in prompt:
        answer = ""
    else:
        try:
            client = openai.OpenAI(base_url=os.environ.get('OPENAI_PROXY'))
            completion = client.chat.completions.create(**prompt)
            choice = completion.choices[0]
            answer = choice.message.content
        except Exception as e:
            print(f"OpenAI error: {e}")
            answer = json.dumps({"error": str(e)})

    ai_grading = AiGrading(
        attempt_id=attempt.id,
        grading_prompter_id=prompter.id,
        input=input_data,
        prompt=json.dumps(prompt),
        output=answer,
    )
    db.session.add(ai_grading)
    db.session.commit()

    return ai_grading


def run_in_sandbox(code, function_name, kwargs):
    queue = Queue()

    def exec_and_send_result():
        try:
            exec(code)
            data = dict(kwargs)
            result = locals()[function_name](data)
        except Exception as e:  # noqa: BLE001
            print("exec_and_send_result error:", e)
            result = {"error": traceback.format_exc()}
        queue.put(result)

    process = Process(target=exec_and_send_result)
    process.start()

    result = queue.get()
    process.join()
    return result


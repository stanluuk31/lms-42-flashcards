from ..app import db, app
from datetime import datetime, timedelta
import json
import os
import sqlalchemy as sa
import shutil
from sqlalchemy.ext.mutable import MutableList


FORMATIVE_ACTIONS = {
    "passed": "The student passes.",
    "repair": "The student needs to correct some work.",
    "failed": "The student needs to start anew.",
}


class Attempt(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    __table_args__ = (
        sa.Index('attempt_status_index', 'status', 'student_id'),
        sa.Index('attempt_student_submit', 'student_id', 'submit_time'),
        sa.UniqueConstraint('student_id', 'node_id', 'number'),
    )

    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    student = sa.orm.relationship("User", foreign_keys="Attempt.student_id", back_populates="attempts")

    number = sa.Column(sa.Integer, nullable=False)

    node_id = sa.Column(sa.String, nullable=False)
    variant_id = sa.Column(sa.SmallInteger, nullable=False)

    start_time = sa.Column(sa.DateTime, default=datetime.utcnow, nullable=False)
    deadline_time = sa.Column(sa.DateTime)
    upload_time = sa.Column(sa.DateTime)
    submit_time = sa.Column(sa.DateTime)

    status = sa.Column(sa.String, nullable=False) # awaiting_approval, in_progress, needs_grading, needs_consent, passed, repair, failed, paused, awaiting_recording
    finished = sa.Column(sa.String) # yes, deadline, forfeit, accidental

    credits = sa.Column(sa.Integer, nullable=False, default=0)  # noqa: A003
    
    avg_days = sa.Column(sa.Float, nullable=False)

    alternatives = sa.Column(sa.JSON, default={}, nullable=False)

    done_objectives = sa.Column(MutableList.as_mutable(sa.ARRAY(sa.Integer)), default=[])

    total_hours = sa.Column(sa.Float, nullable=False, default=0)
    
    record_status = sa.Column(sa.String) # paused, in_progress, error, finished, removed
    max_screenshot_id = sa.Column(sa.Integer, default=0, server_default="0", nullable=False)
    last_screenshot_time = sa.Column(sa.DateTime)

    data_deleted = sa.Column(sa.Boolean, nullable=False, default=False)

    @property
    def hours(self):
        """Returns the number of hours spent on the assignment, excluding structural absent days and regular
        off-time (such as evenings, weekends and vacations).
        We assume that when submit_time is set, the `total_hours` property will be set to our return value.
        As our return value shouldn't change from then on, we will just return `total_hours`.
        """
        if self.submit_time is not None:
            return self.total_hours
        leave_days = {ad.date for ad in AbsentDay.query.filter_by(user_id=self.student_id, exempt_progress=True)}
        return self.total_hours + working_days.get_working_hours_delta(self.start_time, datetime.utcnow(), leave_days)

    @property
    def latest_grading(self):
        return self.gradings.order_by(Grading.id.desc()).first()

    @property
    def directory(self):
        return os.path.join(app.config['DATA_DIR'], 'attempts', f"{self.node_id}-{self.student_id}-{self.number}")

    @property
    def node_name(self):
        nodes = curriculum.get('nodes_by_id')
        if self.node_id not in nodes:
            return self.node_id
        node = nodes[self.node_id]
        name = node["name"]
        module = curriculum.get('modules_by_id')[node["module_id"]]
        if module and module.get("name"):
            name = module.get("short", module.get("name")) + " ➤ " + name
        return name

    @property
    def module_name(self):
        nodes = curriculum.get('nodes_by_id')
        if self.node_id in nodes:
            module = curriculum.get('modules_by_id')[nodes[self.node_id]["module_id"]]
            if module and module.get("name"):
                return module.get('short', module.get("name"))
        return self.node_id

    def write_json(self):
        d = utils.model_to_dict(self)
        d["student"] = {
            "full_name": self.student.full_name,
            "email": self.student.email,
        }
        d["gradings"] = [utils.model_to_dict(grading) for grading in self.gradings]
        with open(os.path.join(self.directory, "attempt.json"), "w") as file:
            file.write(json.dumps(d,indent=4))
        
    def delete_data(self):
        self.data_deleted = True
        db.session.commit()
        shutil.rmtree(self.directory, ignore_errors=True)


def get_notifications(attempt, grading):
    if attempt.status == "in_progress":
        msg = "In progress for"
    elif attempt.status == "paused":
        msg = "Paused after"
    elif attempt.finished == "forfeit":
        msg = "Given up after"
    elif attempt.finished == "deadline":
        msg = "Submitted due to deadline after"
    elif attempt.finished == "accidental":
        msg = "Cancelled accidental attempt after"
    else:
        msg = "Finished after"

    notifications = [
        f"{msg} {round(attempt.hours / working_days.DAY_HOURS, 1)} working days."
    ]

    if attempt.submit_time:
        notifications.append(f"Submitted at {utils.utc_to_display(attempt.submit_time)}.")

    if attempt.deadline_time:
        if attempt.status == "in_progress":
            notifications.append(f"Deadline: {utils.utc_to_display(attempt.deadline_time)}.")

        exceed = working_days.get_working_hours_delta(attempt.deadline_time, attempt.submit_time or datetime.utcnow())
        if exceed > 0:
            notifications.append(f"<strong>Deadline exceeded</strong> by {round(exceed, 1)} hours!")

    if grading:
        notifications.append(f"Graded {grading.grade} by {grading.grader.full_name} on {utils.utc_to_display(grading.time)}.")
        if grading.needs_consent:
            notifications.append("Awaiting consent by a second examiner.")
        else:
            if grading.consent_user_id:
                notifications.append(f"Consent given by {grading.consenter.full_name} on {utils.utc_to_display(grading.consent_time)}.")
            if grading.grade_motivation:
                notifications.append(f"Teacher feedback:<div class='notification is-info'>{utils.unsafe_markdown_to_html(grading.grade_motivation)}</div>")
            if attempt.status in FORMATIVE_ACTIONS:
                notifications.append(f"<strong>{FORMATIVE_ACTIONS[attempt.status]}</strong>")

    return notifications


from . import curriculum
from .grading import Grading
from .. import utils, working_days
from .user import AbsentDay
from ..app import schedule_job


def delete_old_data():
    attempts = Attempt.query \
        .filter_by(data_deleted=False) \
        .filter(datetime.now() - Attempt.submit_time >= sa.sql.expression.case([
            (Attempt.credits > 0, timedelta(days=8*365)),
        ], else_=timedelta(days=365))) \
        .all()

    for attempt in attempts:
        attempt.delete_data()


schedule_job(func=delete_old_data, trigger="cron", hour=4, minute=15, second=0)

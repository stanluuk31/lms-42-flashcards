import os
import pickle
from ..assignment import Assignment
from copy import deepcopy

cur_cache_stat = None
cur_cache_data = None

def get(key=None):
    global cur_cache_stat  # noqa: PLW0603
    global cur_cache_data  # noqa: PLW0603

    stat = os.stat('curriculum.pickle')
    if cur_cache_data is None or cur_cache_stat is None or stat.st_size != cur_cache_stat.st_size or stat.st_mtime != cur_cache_stat.st_mtime:
        cur_cache_stat = stat
        with open('curriculum.pickle', 'rb') as file:
            cur_cache_data = pickle.load(file)

    return cur_cache_data.get(key) if key is not None else cur_cache_data


def get_periods_with_status(student):
    if student is None:
        return get('periods')
    
    attempts_by_node = {}
    for attempt in Attempt.query.filter_by(student_id=student.id).order_by(Attempt.node_id, Attempt.number.asc()):
        if attempt.node_id not in attempts_by_node:
            attempts_by_node[attempt.node_id] = []
        attempts_by_node[attempt.node_id].append(attempt)

    periods = deepcopy(get('periods'))
    
    for period in periods.values():  # noqa: PLR1702
        for module in period:
            module['status'] = 'wip' if module.get('wip') else 'future'
            next_node_status = 'wip' if module.get('wip') else 'startable'

            for node in module['nodes']:
                status = next_node_status
                attempts = attempts_by_node.get(node['id'], [])
                if len(attempts):
                    module['status'] = 'in_progress'

                attempt_statuses = [attempt.status for attempt in attempts]
                if 'in_progress' in attempt_statuses:
                    status = 'in_progress'
                elif 'paused' in attempt_statuses:
                    status = 'paused'
                elif 'passed' in attempt_statuses:
                    status = 'passed'
                    if node is module['nodes'][-1]:
                        # A topic is ready when its last node is passed
                        module['status'] = 'ready'
                elif 'needs_grading' in attempt_statuses or 'needs_consent' in attempt_statuses:
                    status = 'needs_grading'
                elif status == 'startable':
                    for dep_id in node['depend']:
                        dep_statuses = [attempt.status for attempt in attempts_by_node.get(dep_id, [])]
                        if 'passed' in dep_statuses:
                            pass
                        elif 'needs_grading' in dep_statuses or 'needs_consent' in dep_statuses:
                            status = 'almost_startable'
                        else:
                            status = 'future'
                            break
                    
                if status == 'needs_grading' and next_node_status == 'startable':
                    next_node_status = 'almost_startable'
                elif status != 'passed':
                    next_node_status = 'future'

                node['attempts'] = attempts
                node['status'] = status

    return periods


def get_flashcards_from_node(node):
    """Searches for flashcards in the node

    Args:
        node (dict): the assignment node

    Returns:
        list(dict): the collection of flashcards
    """
    flashcards = []
    n = 1
    while True:
        data = node.get(f"assignment{n}")
        if not data:
            break
        if 'flashcards' in node[f"assignment{n}"]:
            flashcards += node[f"assignment{n}"]['flashcards']
        n += 1

    return flashcards

def get_node_with_status(node_id, student):
    # TODO: this could and should be faster!
    node = get('nodes_by_id').get(node_id)

    if not node:
        if not student:
            return None
        attempts = Attempt.query.filter_by(student_id=student.id, node_id=node_id).order_by(Attempt.number.asc()).all()
        if not attempts:
            return None
        # This lesson no longer exists, but the student does have attempts... Fake it!

        try:
            node = Assignment.load_from_directory(attempts[-1].directory).node
            node["name"] += " (DEPRECATED)"
        except FileNotFoundError:
            node = {"name": "DEPRECATED and DELETED"}

        node = Assignment.load_from_directory(attempts[-1].directory).node
        
        for n in node:
            print(n)
            if isinstance(node[n], dict) and 'flashcards' in node[n]:
                print("\n in ", n)
                node['flashcards'] = node[n]['flashcards']
                break
        node["flashcards"] = get_flashcards_from_node(node)
        node["name"] += " (DEPRECATED)"
        node["attempts"] = attempts
        node["status"] = "DEPRECATED"
        return node
    periods = get_periods_with_status(student)
    module = periods[node['period']][node['module_index']]
    node = module['nodes'][node['node_index']]

    # The number of other topics (modules) the student currently has in progress.
    # Set to 0 if the topic for this lesson is already open.
    node["in_progress_topics"] = 0
    if module.get("status") == "future" and module.get("count_as_in_progress", True):
        for period in periods.values():
            for topic in period:
                if topic.get("status") == "in_progress" and topic.get("count_as_in_progress", True):
                    node["in_progress_topics"] += 1
    return node


def get_previous_node(node):
    if node['node_index'] > 0:
        return get('periods')[node['period']][node['module_index']]["nodes"][node['node_index']-1]
    return None


from .attempt import Attempt

from ..app import db
import sqlalchemy as sa


class Flashcard(db.Model):
    id = sa.Column(sa.String, primary_key=True)

    question = sa.Column(sa.String, nullable=False)
    answers = sa.Column(sa.ARRAY(sa.String), nullable=False)

    users = sa.orm.relationship("UserFlashcard", back_populates="flashcard")

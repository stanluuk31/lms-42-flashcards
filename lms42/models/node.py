from ..app import db
import sqlalchemy as sa

class Node(db.Model):
    __tablename__ = "nodes"
    id = sa.Column(sa.Text, primary_key=True)
    info = sa.Column(sa.dialects.postgresql.JSON, nullable=False)

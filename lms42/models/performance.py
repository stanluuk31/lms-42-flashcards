from ..app import db
import sqlalchemy as sa
import datetime
from collections import deque
from backports.cached_property import cached_property
from .. import working_days

LAST_DAY_COUNT = 10


class Performance:

    def __init__(self, user):

        self.user = user
        self.time_logs = TimeLog.query.filter_by(user_id=user.id).order_by(TimeLog.date)
        self.absent_days = {ad.date: ad for ad in AbsentDay.query.filter_by(user_id=user.id)}

        attempts_sql = """
        select max(a.avg_days) avg_days, date(min(a.submit_time))::date as submit_date
        from attempt a
        join "user" u on a.student_id=u.id
        where a.status='passed' and a.student_id = :student_id
        group by a.node_id
        order by min(a.submit_time)
        """
        with db.engine.connect() as dbc:
            self.attempts = dbc.execute(sa.sql.text(attempts_sql), student_id=user.id)


    @cached_property
    def last_days(self):
        last_time_logs = self.time_logs[-LAST_DAY_COUNT-1:]
        today = datetime.date.today()
        day = working_days.offset(today, -LAST_DAY_COUNT)
        results = []
        while day <= today:
            last_day = {
                "date": day,
                "reason": self.absent_days.get(day),
            }
            time_log = next(iter([time_log for time_log in last_time_logs if time_log.date == day]), None)
            if time_log:
                last_day.update({
                    "start": time_log.start_time.strftime("%H:%M"),
                    "end": time_log.end_time.strftime("%H:%M"),
                    "hours": time_log.hours,
                    "suspicious": time_log.suspicious,
                })
            results.append(last_day)
            day = working_days.offset(day, +1)
        for _ in range(14):
            if day in self.absent_days:
                results.append({
                    "date": day,
                    "reason": self.absent_days.get(day),
                })
            day = working_days.offset(day, +1)

        return results


    @cached_property
    def octants(self):
        attempts = deque(self.attempts)
        time_logs = deque(self.time_logs)

        last_year_octant = working_days.get_current_octant()
        if time_logs:
            year_octant = working_days.get_current_octant(time_logs[0].date)
        else:
            year_octant = last_year_octant

        results = []

        while year_octant <= last_year_octant:
            octant_start, octant_end = working_days.get_octant_dates(*year_octant, q4="extra_week")

            octant = {
                "year": year_octant[0],
                "octant": year_octant[1],
                "yq": str(year_octant[0])[-2:] + "." + str(year_octant[1]),
                "first": octant_start,
                "last": octant_end - datetime.timedelta(days=1),
                "hours": 0,
                "progress": 0,
                "days": 0,
            }

            while attempts and attempts[0]["submit_date"] < octant_end:
                attempt = attempts.popleft()
                octant["progress"] += attempt["avg_days"]

            while time_logs and time_logs[0].date < octant_end:
                time_log = time_logs.popleft()
                octant["hours"] += time_log.hours

            today = datetime.date.today()
            for day in working_days.date_range(octant_start, octant_end):
                if day <= today and working_days.is_working_day(day):
                    octant["days"] += 1

            results.append(octant)

            if year_octant[1] < 8:
                year_octant = (year_octant[0], year_octant[1]+1)
            else:
                year_octant = (year_octant[0]+1, 1)

        return results



from .user import TimeLog, AbsentDay

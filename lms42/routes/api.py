from ..app import db, app, csrf
from ..models import curriculum
from ..models.api_token import ApiToken
from ..models.attempt import Attempt
from ..models.user import User
from ..models.curriculum import get as get_curriculum
from flask_login import login_required, current_user
from functools import wraps
import flask
import subprocess
import os
import shutil
import datetime
import yaml


MIN_CLI_VERSION = 17


@app.route('/api/authorize', methods=['GET'])
@login_required
def api_authorize():
    # TODO: add an 'are you sure' prompt
    token = flask.request.args.get('token')
    host = flask.request.args.get('host')
    assert token and host

    db.session.query(ApiToken).filter_by(user_id=current_user.id, host=host).delete()
    api_token = ApiToken(token=token, user=current_user, host=host)
    db.session.add(api_token)
    db.session.commit()
    flask.flash("API client has been authorized!")
    return flask.redirect('/')


def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        auth = flask.request.headers.get('Authorization')
        if auth:
            api_token = db.session.query(ApiToken).filter_by(token=auth).first()
            if api_token:
                flask.request.user = api_token.user
                return f(*args, **kwargs)
        return "Login required.", 401

    return decorated_function


def require_upgrade(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if int(flask.request.args.get('v', '0')) < MIN_CLI_VERSION:
            return "CLI update required", 418 # I am a teapot
        return f(*args, **kwargs)
    return decorated_function


@app.route('/api/attempts/current', methods=['GET'])
@token_required
@require_upgrade
def api_current_get():
    attempt = Attempt.query.filter_by(student_id=flask.request.user.id, status="in_progress").first()
    if attempt:
        attempt = get_attempt_info(flask.request.user.short_name, attempt)
    return flask.jsonify(attempt)


@app.route('/api/attempts/<spec>', methods=['GET'])
@token_required
def api_list_attempts_for_teacher(spec):
    student, node_id, number = parse_spec(spec)
    if not student:
        student = flask.request.user.short_name

    if not flask.request.user.is_teacher and flask.request.user.short_name != student:
        return "Permission denied", 403

    student = User.query.filter_by(short_name = student).first()
    if not student:
        return "No such student", 404

    attempts = Attempt.query.filter_by(student_id = student.id)

    if node_id:
        attempts = attempts.filter_by(node_id = node_id).order_by(Attempt.submit_time.desc())
    else:
        attempts = attempts.order_by(Attempt.submit_time)
    
    if number:
        attempts = attempts.filter_by(number = number)

    if not node_id:
        attempts = attempts.filter(Attempt.status.in_(['needs_grading','needs_consent']))

    return flask.jsonify([get_attempt_info(student.short_name, attempt) for attempt in attempts])


def parse_spec(spec):
    student = spec
    node_id = None
    number = None

    split = student.split('@', 1)
    student = split[0]
    if len(split) == 2:
        split = split[1].split(':', 1)
        node_id = split[0]
        if len(split) == 2:
            number = int(split[1])
    return student, node_id, number


def get_attempt_info(student, attempt):
    node = curriculum.get('nodes_by_id').get(attempt.node_id)
    return {
        'attempt_id': attempt.id,
        'node_id': attempt.node_id,
        'module_id': node['module_id'] if node else None,
        'variant_id': attempt.variant_id,
        'period': node['period'] if node else None,
        'path': node['module_id'] + "/" + attempt.node_id if node else attempt.node_id,
        'number': attempt.number,
        'status': attempt.status,
        'spec': f"{student}@{attempt.node_id}:{attempt.number}",
    }


@app.route('/api/attempts/<spec>/submission', methods=['GET'])
@token_required
def download_submission(spec):
    student, node_id, number = parse_spec(spec)

    if not flask.request.user.is_teacher and student != flask.request.user.short_name:
        return "Permission denied", 403

    student = User.query.filter_by(short_name = student).first()
    if not student:
        return "No such student", 404

    attempt = Attempt.query.filter_by(node_id=node_id, student_id=student.id, number=number).first()

    if not flask.request.user.is_teacher and attempt.credits != 0:
        return "Cannot download exams", 403

    tar = subprocess.Popen(['tar', 'czC', attempt.directory+"/submission", '.'], stdout=subprocess.PIPE)

    return flask.Response(tar.stdout, content_type='application/gzip')


@app.route('/api/attempts/<attempt_id>/template', methods=['GET'])
@token_required
def api_download_template(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    if not attempt or attempt.student_id != flask.request.user.id:
        return "Permission denied", 403
    if attempt.status != "in_progress":
        return "Only in-progress templates may be downloaded", 403
    tar = subprocess.Popen(['tar', 'czC', f'{attempt.directory}/template', '.'], stdout=subprocess.PIPE)

    return flask.Response(tar.stdout, content_type='application/gzip')


@app.route('/api/attempts/<attempt_id>/submission', methods=['PUT','POST'])
@token_required
@csrf.exempt
def api_upload_submission(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    if not attempt or attempt.student_id != flask.request.user.id:
        return "Permission denied", 403
    if attempt.status != "in_progress":
        return "Only in-progress submissions may be uploaded", 403

    node = curriculum.get('nodes_by_id')[attempt.node_id]
    if not node.get('upload', True):
        return "The current lesson doesn't require an upload", 404

    tmp = f"{attempt.directory}/submission-{os.getpid()}"
    dst = f"{attempt.directory}/submission"
    try:
        os.mkdir(tmp)
        tar = subprocess.Popen(['tar', 'xzC', tmp, '.'], stdin=subprocess.PIPE)
        assert tar.stdin
        count = 0
        while True:
            chunk = flask.request.stream.read(64*1024)
            if not chunk:
                break
            count += len(chunk)
            tar.stdin.write(chunk)
        tar.stdin.close()
        tar.wait()
        if tar.returncode:
            raise Exception("tar finished with an error")

        try:
            shutil.rmtree(dst)
        except:  # noqa: E722
            pass
        os.rename(tmp, dst)

        attempt.upload_time = datetime.datetime.utcnow()
        db.session.commit()
        attempt.write_json()

        return flask.jsonify({"transferred": count})

    except Exception as e:  # noqa: BLE001
        try:
            shutil.rmtree(tmp)
        except:  # noqa: E722
            pass
        return f"Upload failed: {e}", 400


@app.route('/api/node-paths', methods=['GET'])
@require_upgrade
def api_get_curriculum_structure():
    rename_index = int(flask.request.args.get("rename_index", "0"))
    result = {}
    for module_id, module in get_curriculum('modules_by_id').items():
        for node in module["nodes"]:
            result[node["id"]] = module_id+"/"+node["id"]

    with open("assignments/renames.yaml") as file:
        renames = (yaml.load(file, Loader=yaml.FullLoader) or [])[rename_index:]

    for item in reversed(renames):
        assert len(item) == 1
        old, new = next(iter(item.items()))
        if new not in result:
            raise AssertionError(f"Assignment '{new}' does not exist")
        if old in result:
            raise AssertionError(f"Assignment '{old}' already exists exist")
        result[old] = result.pop(new)

    return flask.jsonify([result, len(renames)])

"""Provides routes for the company overview system."""

import os
import random
import string
import sys

import flask
from flask_login import current_user, login_required
from werkzeug.datastructures import FileStorage
import sqlalchemy as sa
from ..email import send as send_email

from ..app import app, db, retry_commit
from ..forms.company import AddCompanyForm, EditCompanyForm, EditCompanyFormTeacher, CompanyContactForm, InternshipForm, InternshipEditForm, CompanyFilterForm
from ..forms.safari import AddSafariForm, AddSafariOfferForm
from ..models.company import Company, CompanyContact, Internship
from ..models.safari import Safari, SafariOffer, SafariRegistration
from ..utils import role_required


LOGO_DIR = app.config['DATA_DIR'] + '/logos'
os.makedirs(LOGO_DIR, exist_ok=True)


def assert_authorization(required_company_id=None):
    valid_teacher = current_user.is_authenticated and current_user.is_teacher
    if required_company_id != flask.session.get('company_id') and not valid_teacher:
        print("Company permission denied", required_company_id, flask.session.get('company_id'), valid_teacher)
        flask.abort(403)


def save_logo(company, form):
    """Save the logo from the form into the filesystem.

    Args:
        company (Company): The Company object that the logo belongs to.
        form (FlaskForm): The Form object that contains the file in its 'logo'.

    Returns:
        str: The filename that the logo was saved with.
    """

    if isinstance(form.logo.data, FileStorage):
        logo_file = form.logo.data
        rand_str = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        filename = f'{rand_str}.png'

        logo_file.save(os.path.join(LOGO_DIR, filename))

        return filename

    return company.logo


@app.route('/companies', methods=['GET', 'POST'])
def show_companies():
    if company_id := flask.session.get('company_id'):
        return flask.redirect(f'/companies/{company_id}')
    if not current_user.is_authenticated:
        return flask.redirect("/user/login?next=/companies")

    companies = Company.query.order_by(Company.name) # func.random())
    if not current_user.is_teacher:
        companies = companies.filter_by(company_publish=True, teacher_publish=True)

    filtered_companies, filter_text = filter_companies(companies, flask.request.args)
    print(f"sql: #{filtered_companies}")

    filter_form = CompanyFilterForm(flask.request.args)
    filter_form.city.choices += ((city, city,) for city in sorted({company.city for company in companies if company.city})) # appending to static list attribute will make an instance attribute

    safaris = None
    if current_user.is_authenticated and current_user.is_teacher:
        safaris = Safari.query.order_by(Safari.id.desc())

    return flask.render_template('companies.html', companies=filtered_companies, safaris=safaris, filter_form=filter_form, filter_text=filter_text)


@app.route('/companies/<int:company_id>', methods=['GET'])
def show_company(company_id):
    company = Company.query.get(company_id)
    if not company:
        flask.abort(404)

    is_admin = company.id == flask.session.get('company_id') or (current_user.is_authenticated and current_user.is_teacher)
    accessible = is_admin or (current_user.is_authenticated and company.company_publish and company.teacher_publish) or current_user.id in [internship.user_id for internship in company.internships]
    if not accessible:
        return flask.redirect("/user/login?next=/companies")
    
    student_contact = any(contact.student_visible for contact in company.contacts)
    safari_contact = any(contact.safari_messages for contact in company.contacts)

    safari = Safari.query.order_by(Safari.id.desc()).first()
    internships = None
    if current_user.is_authenticated:
        internships = Internship.query.filter_by(company_id=company_id).order_by(Internship.end_date.desc()).all()

    return flask.render_template('company.html', company=company, is_admin=is_admin, student_contact=student_contact, safari_contact=safari_contact, safari=safari, internships=internships)


@app.route('/companies/add', methods=['GET', 'POST'])
@role_required('teacher')
def add_company():
    company_form = AddCompanyForm()

    if not company_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=company_form,
            title="Add a company"
        )

    company = Company()
    company_form.populate_obj(company)

    db.session.add(company)
    db.session.commit()

    return flask.redirect(flask.url_for('show_companies'))


@app.route('/companies/<int:company_id>/edit', methods=['GET', 'POST'])
def edit_company(company_id):
    assert_authorization(company_id)
    company = Company.query.get(company_id)

    is_teacher = current_user.is_authenticated and current_user.is_teacher
    company_form = EditCompanyFormTeacher(obj=company) if is_teacher else EditCompanyForm(obj=company)

    if not company_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            title='Edit company info',
            form=company_form,
        )

    company_form.populate_obj(company)
    company.logo = save_logo(company, company_form)

    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company_id,
        ),
    )
     

@app.route('/companies/<int:company_id>/contacts/add', methods=['GET', 'POST'])
def add_company_contact(company_id):
    assert_authorization(company_id)

    contact_form = CompanyContactForm()
    if contact_form.validate_on_submit():
        contact = CompanyContact()
        contact_form.populate_obj(contact)
        contact.company_id = company_id
        db.session.add(contact)
        db.session.commit()
        return flask.redirect(f"/companies/{company_id}")

    company = Company.query.get(company_id)
    return flask.render_template('generic-form.html', form=contact_form, title=f"Add contact for {company.name}")


@app.route('/companies/<int:company_id>/contacts/<int:contact_id>/edit', methods=['GET', 'POST'])
def edit_company_contact(company_id, contact_id):
    assert_authorization(company_id)
    contact = CompanyContact.query.filter_by(company_id=company_id, id=contact_id).first()

    contact_form = CompanyContactForm(obj=contact)
    if contact_form.validate_on_submit():
        contact_form.populate_obj(contact)
        db.session.commit()
        return flask.redirect(f"/companies/{company_id}")

    company = Company.query.get(company_id)
    return flask.render_template('generic-form.html', form=contact_form, title=f"Edit contact for {company.name}")


@app.route('/companies/<int:company_id>/contacts/<int:contact_id>/delete', methods=['POST'])
def delete_company_contact(company_id, contact_id):
    assert_authorization(company_id)
    contact = CompanyContact.query.filter_by(company_id=company_id, id=contact_id).first()
    db.session.delete(contact)
    db.session.commit()
    return flask.redirect(f"/companies/{company_id}")


@app.route('/companies/<int:company_id>/delete', methods=['POST'])
@role_required('admin')
def delete_company(company_id):
    company = Company.query.get(company_id)

    db.session.delete(company)
    db.session.commit()

    flask.flash(f'{company.name} deleted.')

    return flask.redirect(flask.url_for('show_companies'))


@app.route('/companies/safaris/<int:safari_id>', methods=['GET'])
@role_required('teacher')
def show_safari(safari_id):
    safari = Safari.query.get(safari_id)
    assert safari

    return flask.render_template('safari.html', safari=safari)


@app.route('/companies/safaris/<int:safari_id>', methods=['POST'])
@role_required('admin')
def admin_safari(safari_id):
    safari = Safari.query.get(safari_id)
    assert safari
    
    if "delete" in flask.request.form:
        db.session.delete(safari)
        db.session.commit()
        notify_companies_cancel(safari)

    elif "remind_companies" in flask.request.form:
        notify_companies_remind(safari)

    elif "start_student_registration" in flask.request.form:
        safari.status = 'student_registration'
        db.session.commit()

    elif "close" in flask.request.form:
        safari.status = 'close'
        db.session.commit()
        notify_companies_closed(safari)

    else:
        flask.abort(403)

    return flask.redirect(flask.request.url)


@app.route('/companies/safaris/add', methods=['GET', 'POST'])
@role_required('admin')
def add_safari():
    add_safari_form = AddSafariForm()

    if not add_safari_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=add_safari_form,
        )

    safari = Safari()
    add_safari_form.populate_obj(safari)

    db.session.add(safari)
    db.session.commit()

    notify_companies_scheduled(safari)

    return flask.redirect(
        flask.url_for(
            'show_safari',
            safari_id=safari.id,
        ),
    )


@app.route('/companies/<int:company_id>/safaris/<int:safari_id>', methods=['GET', 'POST'])
def show_safari_company(company_id, safari_id):
    assert_authorization(company_id)

    safari = Safari.query.get(safari_id)
    if not safari:
        flask.abort(404)

    if safari.status != 'company_registration' and not (current_user.is_authenticated and current_user.is_teacher):
        flask.flash("Company availability signup for this safari has been closed.")
        return flask.redirect(
            flask.url_for(
                'show_company',
                company_id=company_id
            ),
        )

    company = Company.query.get(company_id)
    offer = safari.get_offer(company_id)
    form = AddSafariOfferForm(obj=offer)

    if not form.validate_on_submit():
        return flask.render_template(
            'safari-form.html',
            form=form,
            safari=safari,
            company=company
        )

    if not offer:
        offer = SafariOffer()
        offer.safari_id = safari.id
        offer.company = company
        db.session.add(offer)
    
    form.populate_obj(offer)
    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_company',
            company_id=company.id
        ),
    )


@app.route('/companies/<int:company_id>', methods=['POST'])
@login_required
@retry_commit
def action_company(company_id):
    safari = Safari.query.order_by(Safari.id.desc()).first()
    assert safari.status == 'student_registration' or current_user.is_teacher
    registration = safari.get_registration(current_user.id)

    if "safari_register" in flask.request.form:
        offer = SafariOffer.query.filter_by(safari_id=safari.id, company_id=company_id).first()
        if registration:
            flask.flash("Already registered.")
        elif not offer.available_spots:
            flask.flash("No more spots available.")
        else:
            registration = SafariRegistration(safari_id=safari.id, student_id=current_user.id, company_id=company_id)
            db.session.add(registration)

    elif "safari_deregister" in flask.request.form:
        if not registration or registration.company_id != company_id:
            flask.flash("Not registered.")
        else:
            db.session.delete(registration)

    else:
        flask.abort(403)

    return flask.redirect(flask.request.url)


FOOTER = """Met vriendelijke groet,

Timothy Sealy
Teamleider en docent
Associate degree Software Development
t.e.sealy@saxion.nl | 06 46250417"""

EXPLAIN = """Voor wie de Business Safari nieuw is, of een geheugensteuntje kan gebruiken: Drie keer per jaar sturen wij (de tweejarige Software Development-opleiding van Saxion) onze studenten een dag 'het veld in'. We hopen dat jouw bedrijf een klein groepje studenten zou willen huisvesten. Minimale benodigdheden: een werkplek, wifi en een (junior) ontwikkelaar die beschikbaar wil zijn als gastdame of -heer. De studenten werken in principe aan hun eigen studie-opdrachten, maar krijgen ondertussen natuurlijk wel de ambiance en dagelijkse activiteiten van het bedrijf mee. Het liefst zien we de studenten daarom niet alleen met elkaar in een ruimte, maar ergens waar sfeer te proeven valt. Indien mogelijk zou het mooi zijn als de studenten in sommige activiteiten meegenomen kunnen worden. Denk aan het meekijken bij een stand-up of andere meeting, een rondleiding, een welkomstwoordje, gezamenlijk lunchen, etc. Ook wordt het erg gewaardeerd als de ontwikkelaars van het bedrijf interesse tonen in het werk van de student en wat willen laten zien van hun eigen werk. Maar er hoeft niks!"""


@app.route('/companies/internship/<int:internship_id>/update', methods=['GET', 'POST'])
def edit_internship(internship_id):
    internship = Internship.query.get(internship_id)

    if not internship:
        flask.abort(404)

    internship_edit_form = InternshipEditForm(obj=internship)

    if not internship_edit_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=internship_edit_form,
            title="Edit internship",
            action=f"/companies/internship/{internship_id}/update"
        )

    if internship.user_id == current_user.id or current_user.is_admin:
        internship.review = flask.request.form.get('review')
        internship.start_date = flask.request.form.get('start_date')
        internship.end_date = flask.request.form.get('end_date')
        db.session.commit()
    
    return flask.redirect(f'/companies/{internship.company_id}')


@app.route('/companies/internship/<int:internship_id>/delete', methods=['POST'])
def delete_internship(internship_id):
    internship = Internship.query.get(internship_id)
    if not internship:
        flask.abort(404)

    if current_user.is_admin:
        db.session.delete(internship)
        db.session.commit()
    
    return flask.redirect(f'/companies/{internship.company_id}')


@app.route('/internship/register', methods=['GET', 'POST'])
@login_required
def post_user_register_internship():
    companies = Company.query.order_by(Company.name).all()
    form = InternshipForm()

    choices = [(company.id, company.name) for company in companies]
    choices.append(("-1", "My company is not in the list."))
    form.company_id.choices = choices

    if not form.validate_on_submit():
        return flask.render_template('register-internship.html', form=form, action="/internship/register")

    company_id = flask.request.form.get('company_id')

    if company_id == "-1":
        new_company = Company()
        new_company.name = flask.request.form.get('company_name')

        db.session.add(new_company)
        db.session.commit()

        company_id = new_company.id

        company_contact = CompanyContact()
        company_contact.company_id = company_id
        company_contact.first_name = flask.request.form.get('contactperson_first_name')
        company_contact.last_name = flask.request.form.get('contactperson_last_name')
        company_contact.email = flask.request.form.get('contactperson_email')

        db.session.add(company_contact)

    internship = Internship()
    internship.user_id = current_user.id
    internship.company_id = company_id
    internship.start_date = flask.request.form.get('start_date')
    internship.end_date = flask.request.form.get('end_date')

    db.session.add(internship)
    db.session.commit()

    return flask.redirect(f'/companies/{company_id}')


def get_names_and_emails(company):
    emails = []
    names = []
    for contact in company.safari_contacts:
        emails.append(contact.email)
        names.append(contact.first_name or contact.role or contact.last_name or company.name)

    if len(names) > 1:
        last = names.pop()
        names[-1] += " en " + last
    names = ', '.join(names)
    return names, emails


def notify_companies_scheduled(safari):
    title = "Business Safari in aantocht"

    for company in Company.query.all():
        names, emails = get_names_and_emails(company)
        if not emails:
            continue
        url = company.create_login_link()

        message = f"""Beste {names},

Met plezier kunnen we melden dat er weer een Business Safari aan zit te komen, op {safari.first_date} (en {safari.second_date})!

We hopen dat {company.name} een aantal van onze studenten wil ontvangen. Laat het alsjeblieft even weten via {url} vóór {safari.company_signup_end_date}. Na die datum kunnen studenten inschrijven en vervolgens horen jullie van ons wie je kunt verwachten. Het zou heel fijn zijn als je gelijk even de algemene bedrijfsinformatie (ook gebruikt voor onze stage-database) wil controleren en aanvullen.

{EXPLAIN}

Alvast heel erg bedankt voor jullie hulp!

{FOOTER}
"""
    
        send_email(emails, title, message, fake=False)


def notify_companies_remind(safari):
    title = "Reminder: Business Safari"

    for company in Company.query.all():
        names, emails = get_names_and_emails(company)
        offer = SafariOffer.query.filter_by(company_id=company.id, safari_id=safari.id).first()
        if offer or not emails:
            continue
        url = company.create_login_link()

        message = f"""Beste {names},

Onze Business Safari komt snel dichterbij ({safari.first_date} en {safari.second_date}), maar we hebben nog niet gehoord of jullie deze keer kunnen en willen deelnemen. Zou het lukken om dit vandaag of morgen nog even aan te geven? Dat kan via: {url} - Het zou heel fijn zijn als je gelijk even de algemene bedrijfsinformatie (ook gebruikt voor onze stage-database) wil controleren en aanvullen.

{EXPLAIN}

Alvast heel erg bedankt voor jullie hulp!

{FOOTER}
"""
    
        send_email(emails, title, message, fake=False)


def notify_companies_cancel(safari):
    title = "Business Safari geannuleerd"

    for company in Company.query.all():
        names, emails = get_names_and_emails(company)
        if not emails:
            continue
        message = f"""Beste {names},

Helaas kan de Business Safari van {safari.first_date} en en {safari.second_date} niet doorgaan. Sorry voor het ongemak!

Reply gerust mochten er vragen zijn naar aanleiding hiervan.

{FOOTER}
"""
        send_email(emails, title, message, fake=False)


def notify_companies_closed(safari):
    title = "Business Safari studenten"

    for offer in safari.offers:
        if not offer.spots:
            continue
        names, emails = get_names_and_emails(offer.company)
        if not emails:
            continue
        message = f"""Beste {names},

De inschrijving voor de Business Safari is zojuist gesloten.
"""

        date = safari.second_date if offer.use_alt_day else safari.first_date
        registrations = offer.registrations
        if offer.registrations:
            message += f"\nOp {date} kunnen jullie de volgende student(en) verwachten:\n"

            company = offer.company
            address = f"Adres: {company.address}, {company.city}\n" if company.address else ""

            for registation in registrations:
                student = registation.student
                message += f"- {student.first_name} {student.last_name} <{student.email}>\n"
            message += f"""
De student(en) hebben de volgende informatie ontvangen:

{address}Contactpersoon: {offer.contact_person or '?'}, {offer.contact_person_info or '?'}
{offer.extra_information.strip()}

{EXPLAIN}

Reply gerust mochten er naar aanleiding hiervan vragen zijn. Alvast heel hartelijk bedankt, en wij wensen jullie een geslaagde safari-dag toe!

{FOOTER}
"""

        else:
            message += f"""
Helaas hebben zich voor {date} bij jullie geen studenten ingeschreven. Sorry, kan gebeuren. Volgende keer beter? Reply gerust mochten er naar aanleiding hiervan vragen zijn!

{FOOTER}
"""

        send_email(emails, title, message, fake=False)


def filter_companies(companies, args: dict):
    
    size = args.get('size')
    if size == 'small':
        description = "Small companies"
        companies = companies.filter(Company.developers < 10)
    elif size == 'medium':
        description = "Mid-sized companies"
        companies = companies.filter((Company.developers >= 10) & (Company.developers < 50))
    elif size == 'large':
        description = "Large companies"
        companies = companies.filter(Company.developers >= 50)
    else:
        description = 'All companies'

    if city := args.get('city'):
        companies = companies.filter_by(city=city)
        description += f" in {flask.escape(city)}"

    offer = args.get('offer')
    if offer == 'internship':
        description += " offering internships"
        companies = companies.filter_by(intern_welcome=True)
        
    elif offer in {'safari', 'altsafari'}:
        safari = Safari.query.order_by(Safari.first_date.desc()).first()
        description += f" with {'primary' if offer == 'safari' else 'alternative'} safari day spots"
        if safari and safari.status == 'student_registration':

            companies = companies.join(SafariOffer,
                (SafariOffer.company_id == Company.id) &
                (SafariOffer.safari_id == safari.id) &
                (SafariOffer.use_alt_day == (offer == 'altsafari'))
            ).filter(
                SafariOffer.spots > db.session.query(sa.func.count()).filter(
                    SafariRegistration.safari_id == safari.id,
                    SafariRegistration.company_id == Company.id
                ).correlate(Company)
            )
        else:
            flask.flash("No safari with open registration")
            companies = companies.filter_by(id=0)

    return companies, description


if __name__ == '__main__':
    # bin/sd42 run python -m lms42.routes.company remind
    with app.app_context():
        if len(sys.argv) != 2:
            print("Command expected")
        elif sys.argv[1] == 'remind':
            safari = Safari.query.order_by(Safari.id.desc()).first()
            notify_companies_remind(safari)
        else:
            print("Invalid command")

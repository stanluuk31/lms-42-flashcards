from ..app import db, app
from ..assignment import markdown_to_inline_html
from ..routes.curriculum import get_student
from ..models.user import UserFlashcard
from ..models.flashcard import Flashcard
from ..models import curriculum
from flask_login import login_required, current_user
from flask_wtf.csrf import generate_csrf
import datetime
import flask
import sqlalchemy
import time


@app.route('/flashcards', methods=["GET", "POST"])
def get_cards():
    student = get_student()

    if not student:
        return flask.make_response("Forbidden", 403)
    
    cards = get_flashcards(student.id)

    if len(cards) == 0:
        return flask.render_template("flashcard.html", code_question="Great Job!, No more cards for today!", cards_done=True)
    
    code = markdown_to_inline_html(cards[0].flashcard.question)

    url = "/flashcards/submit"
    start_time = time.time()
    return flask.render_template("flashcard.html", code_question=code, request_method="post", button_text="Submit", token=generate_csrf(), url=url, start_time=start_time)


@app.route('/flashcards/submit', methods=["POST"])
@login_required
def update_card():
    student = get_student()

    if not student:
        return flask.render_template("flashcard.html", code_question="Please log in!")

    duration = time.time() - float(flask.request.form.get('start_time'))
    answer = flask.request.form.get('answer')
    card = get_flashcards(student.id)[0]
        
    reps, easiness_factor, interval = handle_flashcard_answer(card.flashcard.answers, answer, duration, card.easiness_factor, card.repetitions, card.interval)

    # how awesome, after 10 reps the interval can be 42 when the answer is correct and quickly given
    # and after about 10 correct responses the student should remember the answer.
    memorized = reps >= 10 and interval >= 42
    last_v = datetime.datetime.now().date()

    # update card
    card.repetitions = reps
    card.last_viewed = last_v
    card.interval = interval
    card.next_view = last_v + datetime.timedelta(interval)
    card.memorized = memorized
    card.easiness_factor = easiness_factor
    
    db.session.commit()

    # right answer
    if answer in card.flashcard.answers:
        return flask.redirect("/flashcards")
    
    code = markdown_to_inline_html(card.flashcard.question)
    return flask.render_template("flashcard.html", code_question=code, request_method="get", button_text="Next", answers=card.flashcard.answers, token=generate_csrf(), url="/flashcards")
    

@app.route('/curriculum/<node_id>/flashcards/<card_set_index>/<card_index>/validate', methods=["POST"])
@login_required
def validate_flashcard_answer(node_id, card_set_index, card_index):
    """The route that handles the form of the flashcard that is submitted,
        controls the answer and updates the question in the database accordingly.

    Args:
        node_id (str): the name of the node(assignment)
        card_set_index (str): index of the card list in the assignment
        card_index (str): index of the current card in the card list

    Returns:
        Redirects to the next template if the answer is correct,
        else it renders the right answers for the question.
    """
    # first get the time as quick as possible
    duration = time.time() - float(flask.request.form.get('start_time'))

    student = get_student()
    node = curriculum.get_node_with_status(node_id, student)
    assert node

    card_index = int(card_index)
    card_set = int(card_set_index)
    submitted_card = node['flashcards'][card_set][card_index]
    card_id = get_flashcard_id(submitted_card)
    
    if student:

        answer = flask.request.form.get('answer')

        # only the first time the card is seen it is added to the database with it's values,
        # after the first time the user can still see the card but it is not updated in the database anymore
        if not is_card_already_seen(student.id, card_id):
            reps, easiness_factor, interval = handle_flashcard_answer(submitted_card.get('answer'), answer, duration)
            insert_user_flashcard(student, card_id, reps, easiness_factor, interval)

        if answer not in submitted_card.get('answer'):
            # load 'wrong answer' page
            url = f'/curriculum/{node_id}/flashcards/{card_set}/{card_index + 1}'
            code = markdown_to_inline_html(submitted_card.get('question'))
            return flask.render_template("flashcard.html", button_text="Next", code_question=code, url=url, answers=submitted_card.get('answer'), num=card_index + 1, request_method='get')
        
    return flask.redirect(f'/curriculum/{node_id}/flashcards/{card_set}/{card_index + 1}')


@app.route('/curriculum/<node_id>/flashcards/<cardset_i>/<card_index>', methods=["GET"])
def display_flashcard(node_id, cardset_i, card_index):
    """Displays the flashcard in the modal

    Args:
        node_id (str): the name of the node(assignment)
        card_set_index (str): index of the card list in the assignment
        card_index (str): index of the current card in the card list

    Returns:
        renders the flashcard correctly
    """
    student = get_student()

    if not student and not current_user.is_authenticated:
        return flask.render_template("flashcard.html", code="You need to be logged in to view the flashcards!")
    
    node = curriculum.get_node_with_status(node_id, student)
    assert node

    # since the button appeared, 'flashcards' is always part of node
    flashcards = node['flashcards']
    # the last flashcard of the assignment is done
    if int(card_index) > len(flashcards[int(cardset_i)]) - 1:
        return flask.render_template("flashcard.html", code_question="Good work! All cards are answered!.", cards_done=True)

    start_time = time.time()
    requested_card = node['flashcards'][int(cardset_i)][int(card_index)]
    # does not upload to db if card already is in db
    flashcard_to_db(requested_card)
    code = markdown_to_inline_html(requested_card.get('question'))
    return flask.render_template('flashcard.html', code_question=code, token=generate_csrf(), request_method='post', url=f"../flashcards/{int(cardset_i)}/{card_index}/validate", button_text="Answer", start_time=start_time)


def get_flashcards(student_id):
    # returns the cards scheduled for today or before, that are not yet memorized.

    current_date = datetime.datetime.now().date()
    return db.session.query(UserFlashcard).filter(
        UserFlashcard.user_id == student_id,
        UserFlashcard.next_view <= current_date,
        ).filter_by(
        memorized=False
    ).all()


def handle_flashcard_answer(card_answers, answer, duration, easiness_factor=2.5, repetition_num=0, interval=0):
    # Calculates the values of the flashcard data that needs to be updated.
    q = calculate_q_score(answer, card_answers, duration)
    repetition_number, easiness_factor, interval = apply_sm2(q, repetition_num, easiness_factor, interval)
    return repetition_number, easiness_factor, interval


def calculate_q_score(input_answer, card_answers, duration):
    # calculates the Q score based on if the answer is correct and the time it took to fill in.

    q = 0
    # right answer
    if input_answer in card_answers:
        if duration < 8:
            q = 5
        elif duration < 16:
            q = 4
        elif duration >= 16:
            q = 3
    # wrong answer
    else:
        closest_answer = card_answers[0]
        # get the levenshtein_distance
        levenshtein_distance = 99 # max distance
        for original_ans in card_answers:
            # get the answer with the biggest length

            l_d = apply_levenshtein(original_ans, input_answer)
            if l_d < levenshtein_distance:
                closest_answer = original_ans
                levenshtein_distance = l_d

        max_distance = len(closest_answer) // 3

        # small typo is a little bit of a good answer
        if levenshtein_distance > max_distance:
            q = 0
        
        else:
            max_score = 3.5 # max scoring 3.5 point by a typo
            ratio = levenshtein_distance / len(closest_answer)
            q = round((1 - ratio) * max_score, 1)
    return q


def apply_sm2(q_score, repetition_num, easiness_factor, interval):
    """SM-2 Algorithm implementation, calculates the best time for a student to view the card again (interval),
    and returns the easiness factor and repetition number that need to be constantly up to date.

    Args:
        q_score (float): This is a score how well the question is answered from 0-5,
        repetition_num (int): number of times the card is seen
        easiness_factor (float): unique easiness factor, which shows how hard the user finds the question.
        interval (int): previous days between viewing the card again

    Returns:
        _type_: _description_
    """
    # correct answer
    if q_score >= 3:
        if repetition_num == 0:
            interval = 1
        elif repetition_num == 1:
            interval = 6
        else:
            interval = round(interval * easiness_factor)

        repetition_num += 1
    # incorrect answer
    else:
        repetition_num = 0
        interval = 1

    easiness_factor = round(easiness_factor + (0.1 - (5 - q_score) * (0.08 + (5 - q_score) * 0.02)), 2)
    easiness_factor = max(easiness_factor, 1.3)
    
    return repetition_num, easiness_factor, interval


def apply_levenshtein(s1, s2):
    # Levenshtein calculates the number of modifications needed to s1(string) in order to get s2(string).
    # create the matrix '*' unpacks the range into a list
    matrix = [[*range(len(s1) + 1)] for _ in range(len(s2) + 1)]
    for i in range(len(s2) + 1):
        matrix[i][0] = i
    for i in range(1, len(s2) + 1):
        for j in range(1, len(s1) + 1):
            matrix[i][j] = min(matrix[i-1][j] + 1, matrix[i][j-1] + 1, matrix[i-1][j-1] + (s2[i-1] != s1[j-1]))

    return matrix[-1][-1]


def get_user_flashcard(student_id, card_id):
    # get a flashcard by user id and card id
    return db.session.query(UserFlashcard).filter(
        UserFlashcard.user_id == student_id,
        UserFlashcard.flashcard_id == card_id).first()


def is_card_already_seen(student_id, card_id):
    # checks if the card is already loaded into the database
    return db.session.query(sqlalchemy.exists()
        .where(UserFlashcard.user_id == student_id)
        .where(UserFlashcard.flashcard_id == card_id)).scalar()


def get_flashcard_id(flashcard):
    # gets the id of the flashcard
    card_id = flashcard.get("id")
    if not card_id:
        return flashcard.get('answer')[0]
    return card_id


def flashcard_to_db(card):
    # check if flashcard is in db already. if not, add it
    # set the id, defaults to first answer
    card_id = get_flashcard_id(card)
    
    # check if already in db
    if Flashcard.query.get(card_id):
        return
        
    new_card = Flashcard(
    id=card_id,
    question=card.get('question'),
    answers=card.get('answer'))

    db.session.add(new_card)
    db.session.commit()


def insert_user_flashcard(student, card_id, repetitions, easiness_factor, interval):
    # insert a new flashcard to the database
    current_date = datetime.datetime.now().date()
    new_user_flashcard = UserFlashcard(
        user_id=student.id,
        flashcard_id=card_id,
        repetitions=repetitions,
        last_viewed=current_date,
        interval=interval,
        next_view=current_date + datetime.timedelta(interval),
        memorized=False,
        easiness_factor=easiness_factor
    )
    db.session.add(new_user_flashcard)
    db.session.commit()

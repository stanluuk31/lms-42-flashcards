from ..app import db, app, schedule_job, retry_commit
import flask
from .. import utils
from ..models.attempt import Attempt
from ..models.grading import Grading
from flask_login import login_required, current_user
from . import discord
from datetime import datetime, timedelta
import os
import shutil


def check_recording() -> None:
    """Checks if the recording is still in progress. By checking the last screenshot time is more than 65 seconds ago.
    The attempt record status will be set to error and the student and all teachers will be notified."""

    attempts = Attempt.query \
        .filter_by(status="in_progress") \
        .filter_by(record_status="in_progress") \
        .filter(datetime.utcnow() - Attempt.last_screenshot_time > timedelta(seconds=65)) \
        .all()

    for attempt in attempts:
                
        attempt.record_status = "error"
        db.session.commit()
        
        discord.message_teachers("Recording error", f"{attempt.student.first_name} {attempt.student.last_name} has stopped recording")
        
        if attempt.student.discord_id is not None:
            discord.send_dm(attempt.student.discord_id, "Recording error", "Screen recording has stopped! Please restart it as soon as possible! (Unless you have handed in your laptop to a teacher.)")
  

schedule_job(check_recording, 'interval', seconds=60, id='check_recordings')


@retry_commit
def remove_expired_screenshots() -> None:
    """For all attempts with screenshots that have been graded at least 5 days ago remove the screenshots."""

    attempts = Attempt.query \
        .filter_by(record_status="finished") \
        .join(Grading, Attempt.id == Grading.attempt_id) \
        .filter(datetime.now() - Grading.time > timedelta(days=5)) \
        .all()

    for attempt in attempts:
        screenshot_location = os.path.join(attempt.directory, "screenshots")
        shutil.rmtree(screenshot_location, ignore_errors=True)
        
        attempt.record_status = 'removed'
        attempt.max_screenshot_id = 0
        db.session.commit()


schedule_job(remove_expired_screenshots,
    trigger='cron',
    day_of_week='mon-sun',
    hour=22,
    minute=0,
    id='remove_expired_screenshots'
)


@app.route('/record/upload', methods=['POST'])
@login_required
@retry_commit
def upload_screenshot():
    # This should always be done, even when we're sending an error messages, as
    # otherwise we appear to be getting connection-reset errors in the client.
    # This appears to be a WebCentral bug we're working around.
    data = flask.request.get_data()

    attempt = current_user.current_attempt
    if attempt is None:
        return flask.jsonify({"status": "stop", "message": "No attempt found"})

    if attempt.record_status == "finished":
        return flask.jsonify({"status": "stop", "message": "Recording stopped"})

    if attempt.record_status == "error":
        attempt.record_status = "in_progress"
        discord.message_teachers("Recording resumed", f"{attempt.student.first_name} {attempt.student.last_name} has resumed recording")
    
    elif attempt.status == "awaiting_recording":
        attempt.status = "awaiting_approval"
        attempt.record_status = "in_progress"

    elif attempt.record_status != "in_progress":
        return flask.jsonify({"status": "stop", "message": "Can't start recording for this assignment"})
    
    # Commit before saving, so as not to leave the transaction open for too long (which may cause clashes).
    attempt.max_screenshot_id += 1
    attempt.last_screenshot_time = datetime.utcnow()
    db.session.commit()

    screenshot_location = os.path.join(attempt.directory, "screenshots")
    if attempt.max_screenshot_id == 1:
        os.makedirs(screenshot_location, exist_ok=True)
    with open(os.path.join(screenshot_location, f"{attempt.max_screenshot_id}.png"), 'wb') as file:
        file.write(data)
    
    return flask.jsonify({"status": "ok", "message": "Screenshot saved"})



@app.route('/record/<int:attempt_id>/screenshot_count', methods=['GET'])
@utils.role_required('teacher')
def screenshot_list(attempt_id: int):
    attempt = Attempt.query.get(attempt_id)
    assert attempt
    return flask.jsonify({"max_id": attempt.max_screenshot_id})


@app.route('/record/<int:attempt_id>/screenshot/<int:screenshot_id>', methods=['GET'])
@utils.role_required('teacher')
def screenshot(attempt_id: int, screenshot_id: int):
    attempt = Attempt.query.get(attempt_id)
    screenshot_location_get = os.path.join(attempt.directory, "screenshots")
    
    create_time = os.path.getctime(os.path.join(screenshot_location_get, f"{screenshot_id}.png"))
    create_time_format = datetime.fromtimestamp(create_time).strftime('%Y-%m-%d %H:%M:%S')

    response = flask.make_response(flask.send_file(os.path.join('../', screenshot_location_get, f"{screenshot_id}.png")))  
    response.headers.extend({'File-Created': create_time_format})

    return response

import signal
import sys
import traceback
import threading

# Have SIGUSR1 cause a thread dump
def dump_threads(signal, frame):
    id_to_name = dict([(th.ident, th.name) for th in threading.enumerate()])
    code = []
    for thread_id, stack in sys._current_frames().items():
        code.append("\n# Thread: %s(%d)" % (id_to_name.get(thread_id, ""), thread_id))
        for filename, lineno, name, line in traceback.extract_stack(stack):
            code.append('File: "%s", line %d, in %s' % (filename, lineno, name))
            if line:
                code.append("  %s" % (line.strip()))
    print("\n".join(code))
signal.signal(signal.SIGUSR1, dump_threads)

# Let the login manager know how to load user objects
from .app import app, _scheduler
from .models.user import User

from flask_login import LoginManager
login_manager = LoginManager()
login_manager.login_view = '/user/login'
login_manager.init_app(app)
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

from .routes import *
from .models import *

_scheduler.start()

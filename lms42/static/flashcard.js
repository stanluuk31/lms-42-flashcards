var start = new Date();

function startTimer(){
    start = new Date();
}
var formField = document.getElementById('flashcardForm');
  
function endTimer(){
    let end = new Date();
    let delta = (end - start) / 1000;

    // Send data using hx-headers attribute
    formField.setAttribute('hx-headers', JSON.stringify({ 'Answer-duration': delta }));
}

function openModal(){
        
    let modal = document.getElementById("flashcardModal");
    let dailyFlashcards = document.getElementById("dailyFlashcards");
    modal.style.display = "block";
    dailyFlashcards.style.display = "none";
}

function closeModal(){
    let modal = document.getElementById("flashcardModal");
    let dailyFlashcards = document.getElementById("dailyFlashcards");
    dailyFlashcards.style.display = "block";
    modal.style.display = "none";

}



formField.addEventListener('load', startTimer);
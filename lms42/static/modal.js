function openModal(){
    let modal = document.getElementById("modalBox");
    let dailyFlashcards = document.getElementById("dailyFlashcards");
    modal.style.display = "block";
    dailyFlashcards.style.display = "none";
}

function closeModal(){
    let modal = document.getElementById("modalBox");
    let dailyFlashcards = document.getElementById("dailyFlashcards");
    modal.style.display = "none";
    dailyFlashcards.style.display = "block";
}
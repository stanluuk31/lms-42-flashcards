'use strict'

async function reviewRecording(attemptId) {

    let response = await fetch(`/record/${attemptId}/screenshot_count`);
    let jsonResponse = await response.json();
    const maxId = jsonResponse['max_id'];
    if (!maxId) {
        alert("No screenshots for this attempt.");
        return;
    }

    const overlay = makeNode(".recording-reviewer", document.body, {click: function(e) { if (e.target===this) removeOverlay() } });
    const timeDisplay = makeNode(".time-display", overlay);
    const screenshot = makeNode("img", overlay);
    const sliderBox = makeNode(".slider-box", overlay);

    const currentLocation = makeNode("span", timeDisplay);
    const timeDisplayText = makeNode("span", timeDisplay);

    let screenshotId = 1;
    const slider = makeNode("input", sliderBox, {
        "type": "range",
        "min": 1,
        "max": maxId,
        "value": screenshotId,
        "input": function(event) {
            screenshotId = Math.round(event.target.value);
            updateImage()
            event.stopPropagation();
        },
        "click": function(event) {
            event.stopPropagation();
            event.target.blur();
        }
    });

    async function updateImage() {    
        const screenshotUrl = `/record/${attemptId}/screenshot/${0|screenshotId}`;

        slider.value = screenshotId;
        screenshot.src = screenshotUrl;
        
        let response = await fetch(screenshotUrl, {method: 'HEAD'});
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
    
        timeDisplayText.innerText = response.headers.get('File-Created');
        currentLocation.innerText = `Screenshot ${0|screenshotId} / ${maxId}`;
    }
        
    function step(amount) {
        screenshotId = Math.min(Math.max(1, screenshotId+amount), maxId);
        updateImage();
    }
    
    function keydownHandler(event) {
        if (event.key === "Escape") {
            removeOverlay();
        } else if (event.key === "ArrowLeft") {
            step(event.ctrlKey ? -10 : -1);
        } else if (event.key === "ArrowRight") {
            step(event.ctrlKey ? 10 : 1);
        } else if (event.key === "ArrowUp") {
            step(10);
        } else if (event.key === "ArrowDown") {
            step(-10);
        }
}
    
    function wheelHandler(event) {
        step(-event.deltaY/10);
        event.preventDefault();
    }

    function removeOverlay() {
        overlay.remove();
        document.body.style.overflow = "auto";
        document.body.removeEventListener("keydown", keydownHandler);
        document.body.removeEventListener("wheel", wheelHandler);
    }

    document.body.addEventListener("keydown", keydownHandler);
    document.body.addEventListener("wheel", wheelHandler);
    document.body.style.overflow = "hidden";

    updateImage();
}

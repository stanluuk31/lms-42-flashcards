(function(){

    let nowE = document.getElementById('sched_now')
    let nextE = document.getElementById('sched_next')
    let timeE = document.getElementById('sched_time')


    if (Notification.permission != 'granted'){
        Notification.requestPermission();
    }

    let timesString = `
        09:00 - 09:30: Train intro: ring.mp3
        09:30 - 11:30: Focus time: ring.mp3
        11:55 - 12:00: Announcements: airhorn.mp3
        12:00 - 12:42: Lunch
        12:42 - 13:30: Forty-two Session: airhorn.mp3
        13:30 - 15:30: Focus time: ring.mp3
        15:30 - 16:00: Train outro (Teachers busy!)
        16:00 - 17:00: Relaxed work (Teachers busy!)
    `;
    
    let timetable = [];
    for(let line of timesString.trim().split("\n")) {
        let [times, description, sound] = line.trim().split(': ');
        times = times.split(' - ');
        timetable.push({start: times[0], end: times[1], description: description, sound: sound});
    }

    function calculateTimeDelta(from, to) {
        from = from.split(':').map(a => parseInt(a));
        to = to.split(':').map(a => parseInt(a));
        return (to[0] - from[0])*60 + to[1]-from[1] + " minutes";
    }

    function play(sample) {
        new Audio(`/static/${sample}`).play()
    }

    function notify(now){
        new Notification(now.description, {
            body: now.description + " until " + now.end,
            icon: '/static/favicon.svg'
        });
    }

    function updateSchedule() {
        var dateTime = new Date();
        time = ("0"+dateTime.getHours()).slice(-2) + ":" + ("0"+dateTime.getMinutes()).slice(-2);
        timeE.innerText = time;

        let nowIndex = 0;
        while(nowIndex+1<timetable.length && time >= timetable[nowIndex+1].start) {
            nowIndex++;
        }

        now = nowIndex < 0 ? null : timetable[nowIndex];
        next = nowIndex + 1 >= timetable.length ? null : timetable[nowIndex+1];

        nowE.innerText = now ? now.description : '';
        nextE.innerText = next ? next.description+" in "+calculateTimeDelta(time, next.start) : "Next: have a great evening!";

        if (now && time==now.start) {
            if (Notification.permission == 'granted'){
                notify(now);
            }
            if (now.sound) play(now.sound);
        }

        setTimeout(updateSchedule, (60.2 - dateTime.getSeconds())*1000);
    }

    updateSchedule();

    // // Reload every 2h
    // setTimeout(function() {
    //     location.reload();
    // }, 2*60*60*1000)
})();

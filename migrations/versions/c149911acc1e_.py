"""empty message

Revision ID: c149911acc1e
Revises: 08db469f6356, f1e84c083823
Create Date: 2024-03-26 14:44:55.377621

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c149911acc1e'
down_revision = ('08db469f6356', 'f1e84c083823')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
